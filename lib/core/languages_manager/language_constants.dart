import 'package:flutter/material.dart';
import '../app_constance/AppKey.dart';
import '../cache_helper/cache_helper.dart';

const String LANGUAGE_CODE = 'languageCode';

//languages code
const String ENGLISH = 'en';
const String ARABIC = 'ar';

Future<Locale> setLocale(String languageCode) async {
  await CacheHelper.saveData(key: AppKey.languageCode, value: languageCode);
  return _locale(languageCode);
}

Future<Locale> getLocale() async {

  String languageCode = CacheHelper.getData(key: AppKey.languageCode) ?? ENGLISH;
  return _locale(languageCode);
}

Locale _locale(String languageCode) {
  switch (languageCode) {
    case ENGLISH:
      return const Locale(ENGLISH, '');

    case ARABIC:
      return const Locale(ARABIC, "");

    default:
      return const Locale(ENGLISH, '');
  }
}


