import 'package:task_manager/core/app_colors/app_colors.dart';
import 'package:task_manager/core/app_styles/app_fonts.dart';

import '../export/app_export.dart';

class CustomEnterData extends StatefulWidget {
  CustomEnterData({
    super.key,
    required this.title,
    this.titleStyle,
    this.alignment,
    this.width,
    this.scrollPadding,
    this.controller,
    this.focusNode,
    this.autofocus = false,
    this.textStyle,
    this.obscureText = false,
    this.textInputAction = TextInputAction.next,
    this.textInputType = TextInputType.text,
    this.maxLines,
    this.hintText,
    this.hintStyle,
    this.prefix,
    this.prefixConstraints,
    this.suffix,
    this.suffixConstraints,
    this.suffixFunction, // Add suffix function property
    this.contentPadding,
    this.borderDecoration,
    this.fillColor,
    this.filled = false,
    this.validator,
    this.onTap,
    this.readOnly = false,
    this.border = true, // Add border property with default value
    this.borderRadius = 8.0, // Add border radius property with default value
    this.focusedColor,
    this.filledColor,
    this.padding,
  });

  final String title;
  final TextStyle? titleStyle;
  final Alignment? alignment;
  final double? width;
  final TextEditingController? scrollPadding;
  final TextEditingController? controller;
  final bool readOnly;
  final FocusNode? focusNode;
  final GestureTapCallback? onTap;
  final bool autofocus;
  final TextStyle? textStyle;
  final bool obscureText;
  final TextInputAction? textInputAction;
  final TextInputType? textInputType;
  final int? maxLines;
  final String? hintText;
  final TextStyle? hintStyle;
  final Widget? prefix;
  final BoxConstraints? prefixConstraints;
  final Widget? suffix;
  final BoxConstraints? suffixConstraints;
  final VoidCallback? suffixFunction;
  final EdgeInsets? contentPadding;
  final InputBorder? borderDecoration;
  final Color? fillColor;
  final bool filled;
  final FormFieldValidator<String>? validator;
  final bool border;
  final double borderRadius;
  final Color? focusedColor;
  final Color? filledColor;
  final EdgeInsets? padding;

  @override
  _CustomEnterDataState createState() => _CustomEnterDataState();
}

class _CustomEnterDataState extends State<CustomEnterData> {
  late TextEditingController _controller;
  late FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _controller = widget.controller ?? TextEditingController();
    _focusNode = widget.focusNode ?? FocusNode();

    _controller.addListener(_onTextChanged);
    _focusNode.addListener(_onFocusChanged);
  }

  @override
  void dispose() {
    _controller.removeListener(_onTextChanged);
    _focusNode.removeListener(_onFocusChanged);
    if (widget.controller == null) {
      _controller.dispose();
    }
    if (widget.focusNode == null) {
      _focusNode.dispose();
    }
    super.dispose();
  }

  void _onTextChanged() {
    setState(() {});
  }

  void _onFocusChanged() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    bool isFocused = _focusNode.hasFocus;
    bool isEmpty = _controller.text.isEmpty;

    Color getColor() {
      if (isFocused) {
        return widget.focusedColor ?? AppColors.accent;
      } else {
        return isEmpty
            ? AppColors.secondary
            : (widget.filledColor ?? AppColors.primary);
      }
    }

    TextStyle getTextStyle() {
      return widget.textStyle?.copyWith(color: getColor()) ??
          regularFont400(color: getColor(), fontSize: 16);
    }

    TextStyle getTitleStyle() {
      return widget.titleStyle?.copyWith(color: getColor()) ??
          regularFont400(color: getColor(), fontSize: 14.w);
    }

    InputBorder getBorder() {
      return widget.borderDecoration ??
          OutlineInputBorder(
            borderRadius: BorderRadius.circular(widget.borderRadius),
            borderSide: widget.border
                ? BorderSide(color: getColor(), width: 1)
                : BorderSide(color: Colors.transparent, width: 1),
          );
    }

    Widget? getSuffixIcon() {
      if (widget.suffix == null) {
        return null;
      }
      if (widget.suffixFunction != null) {
        return GestureDetector(
          onTap: widget.suffixFunction,
          child: IconTheme(
            data: IconThemeData(color: getColor()),
            child: widget.suffix!,
          ),
        );
      } else {
        return IconTheme(
          data: IconThemeData(color: getColor()),
          child: widget.suffix!,
        );
      }
    }

    return Container(
      padding: widget.padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.title,
            style: getTitleStyle(),
          ),
          Gap(4.h),
          widget.alignment != null
              ? Align(
                  alignment: widget.alignment!,
                  child: SizedBox(
                    width: widget.width ?? double.maxFinite,
                    child: TextFormField(
                      readOnly: widget.readOnly,
                      onTap: widget.onTap,
                      scrollPadding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).viewInsets.bottom),
                      controller: _controller,
                      focusNode: _focusNode,

                      onTapOutside: (event) {
                        if (widget.focusNode != null) {
                          widget.focusNode?.unfocus();
                        } else {
                          FocusManager.instance.primaryFocus?.unfocus();
                        }
                      },
                      autofocus: widget.autofocus,
                      style: getTextStyle(),
                      obscureText: widget.obscureText,
                      textInputAction: widget.textInputAction,
                      keyboardType: widget.textInputType,
                      maxLines: widget.maxLines ?? 1,
                      cursorColor: getColor(),
                      decoration: InputDecoration(
                        hintText: widget.hintText ?? "",
                        hintStyle: widget.hintStyle ??
                            regularFont400(
                                color: AppColors.secondary, fontSize: 14.w),
                        prefixIcon: widget.prefix != null
                            ? IconTheme(
                                data: IconThemeData(color: getColor()),
                                child: widget.prefix!,
                              )
                            : null,
                        prefixIconConstraints: widget.prefixConstraints,
                        suffixIcon: getSuffixIcon(),
                        suffixIconConstraints: widget.suffixConstraints,
                        isDense: true,
                        contentPadding: widget.contentPadding ??
                            EdgeInsetsDirectional.only(
                              start: widget.prefix == null ? 16.w : 0,
                              top: 12.h,
                              end: 16.w,
                              bottom: 12.h,
                            ),
                        fillColor: widget.fillColor,
                        filled: widget.filled,
                        border: getBorder(),
                        enabledBorder: getBorder(),
                        focusedBorder: getBorder(),
                      ),
                      validator: widget.validator,
                    ),
                  ),
                )
              : SizedBox(
                  width: widget.width ?? double.maxFinite,
                  child: TextFormField(
                    readOnly: widget.readOnly,
                    onTap: widget.onTap,
                    scrollPadding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    controller: _controller,
                    focusNode: _focusNode,
                    onTapOutside: (event) {
                      if (widget.focusNode != null) {
                        widget.focusNode?.unfocus();
                      } else {
                        FocusManager.instance.primaryFocus?.unfocus();
                      }
                    },
                    autofocus: widget.autofocus,
                    style: getTextStyle(),
                    obscureText: widget.obscureText,
                    textInputAction: widget.textInputAction,
                    keyboardType: widget.textInputType,
                    maxLines: widget.maxLines ?? 1,
                    cursorColor: getColor(),
                    decoration: InputDecoration(
                      hintText: widget.hintText ?? "",
                      hintStyle: widget.hintStyle ??
                          regularFont400(
                              color: AppColors.secondary, fontSize: 14.w),
                      prefixIcon: widget.prefix != null
                          ? IconTheme(
                              data: IconThemeData(color: getColor()),
                              child: widget.prefix!,
                            )
                          : null,
                      prefixIconConstraints: widget.prefixConstraints,
                      suffixIcon: getSuffixIcon(),
                      suffixIconConstraints: widget.suffixConstraints,
                      isDense: true,
                      contentPadding: widget.contentPadding ??
                          EdgeInsetsDirectional.only(
                            start: widget.prefix == null ? 16.w : 0,
                            top: 12.h,
                            end: 16.w,
                            bottom: 12.h,
                          ),
                      fillColor: widget.fillColor,
                      filled: widget.filled,
                      border: getBorder(),
                      enabledBorder: getBorder(),
                      focusedBorder: getBorder(),
                    ),
                    validator: widget.validator,
                  ),
                ),
        ],
      ),
    );
  }
}
