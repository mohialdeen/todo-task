
import '../export/app_export.dart';

class NonChangeIconButton extends StatelessWidget {
  const NonChangeIconButton(
      {super.key, this.onPressed, this.color, required this.icon, this.size,this.splash=true});

  final void Function()? onPressed;

  final Color? color;

  final double? size;
  final IconData icon;
final bool splash;
  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Material(
        color: AppColors.gray,
        child: InkWell(
          splashColor:splash? AppColors.gray:Colors.transparent,
          highlightColor: splash? AppColors.gray:Colors.transparent,
          onTap: () {
            if (onPressed != null) {
              onPressed!();
            }
          },
          child: Icon(
            icon,
            size: size ?? 24.w,
            color: color ?? AppColors.secondary,
          ),
        ),
      ),
    );
  }
}