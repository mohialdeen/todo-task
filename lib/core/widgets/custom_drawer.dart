import 'package:task_manager/core/widgets/custom_list_tile.dart';
import 'package:task_manager/core/widgets/profile_avatar.dart';
import 'package:task_manager/features/profile/presentation/view/user_profile.dart';
import 'package:task_manager/features/settings/presentation/view/settings.dart';
import 'package:task_manager/features/to_do/presentation/view/saved_screen.dart';

import '../../features/auth/presentation/view/log_in_screen.dart';
import '../export/app_export.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    var tr = S.of(context);
    return Drawer(
      backgroundColor: AppColors.background,
      elevation: 0,
      width: 256.w,
      child: Container(
        padding: EdgeInsetsDirectional.symmetric(vertical: 48.h),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ProfileAvatar(
                    image: CacheHelper.getData(key: AppKey.image),
                    size: 52.w,
                    userName: CacheHelper.getData(key: AppKey.username),
                  ),
                  Gap(12.w),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          CacheHelper.getData(key: AppKey.username),
                          style: mediumFont500(
                              color: AppColors.primary, fontSize: 14.w),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          softWrap: false,
                        ),
                        Gap(4.h),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Gap(32.h),
            CustomListTile(
              title: Text(tr.view_profile,
                  style:
                      mediumFont500(color: AppColors.primary, fontSize: 14.w)),
              leading: Icon(
                AppIcons.profile,
                color: AppColors.primary,
                size: 20.w,
              ),
              onTap: () {
                 navigateTo(context: context, screen: UserProfile());
              },
            ),
            CustomListTile(
              leading: Icon(
                AppIcons.saved_outlined,
                color: AppColors.primary,
                size: 20.w,
              ),
              title: Text(tr.saved,
                  style:
                      mediumFont500(color: AppColors.primary, fontSize: 14.w)),
              onTap: () {
                navigateTo(context: context, screen: SavedScreen());
              },
            ),
            CustomListTile(
              leading: Icon(
                AppIcons.settings,
                color: AppColors.primary,
                size: 20.w,
              ),
              title: Text(tr.settings,
                  style:
                      mediumFont500(color: AppColors.primary, fontSize: 14.w)),
              onTap: () {
                   navigateTo(context: context, screen: const Settings());
              },
            ),
            CustomListTile(
              leading: Icon(
                AppIcons.logout,
                color: AppColors.primary,
                size: 20.w,
              ),
              title: Text(tr.log_out,
                  style:
                      mediumFont500(color: AppColors.primary, fontSize: 14.w)),
              onTap: () {
                DBHelper.clearToDoTable();
                DBHelper.clearNotificationTable();
                DBHelper.clearTaskTable();
                CacheHelper.deleteData();
                 navigateAndRemoveUntil(context: context, screen: LogInScreen());
              },
            ),
          ],
        ),
      ),
    );
  }
}
