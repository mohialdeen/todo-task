



import '../export/app_export.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  CustomAppBar({super.key, required this.title, required this.onMenuPressed,this.shadow=true,this.actions});

  String title;
  VoidCallback onMenuPressed;
bool shadow;
  Size get preferredSize => Size.fromHeight(56.h);
  List<Widget>? actions;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:shadow? BoxDecoration(
        boxShadow: [
          AppShadows.appBarBoxShadow(),
        ],
      ):null,
      child: AppBar(
          backgroundColor: AppColors.background,
          titleSpacing: 12.w,
          actions: actions,
          leading: IconButton(
            icon: Icon(
              AppIcons.menu2,
              color: AppColors.primary,
            ),
            onPressed: onMenuPressed,
          ),
          title: Text(
            title,
            style: boldFont700(fontSize: 20.w, color: AppColors.primary),
          )),
    );
  }
}
