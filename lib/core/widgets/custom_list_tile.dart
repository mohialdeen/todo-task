import '../../../../../core/app_colors/app_colors.dart';
import '../export/app_export.dart';

class CustomListTile extends StatelessWidget {
  const CustomListTile(
      {super.key,
      required this.title,
      this.leading,
      this.color,
      this.onTap,
      this.devider = false,
      this.padding,
      this.spaceBetween});

  final Widget title;
  final Widget? leading;
  final Color? color;
  final Function? onTap;
  final bool devider;
  final EdgeInsets? padding;
  final double? spaceBetween;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap!();
        }
      },
      splashColor: AppColors.gray,
      highlightColor: AppColors.gray,
      child: Container(
        width: double.infinity,
        decoration: devider
            ? BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: AppColors.gray, // You can specify the color here
                    width: 1.0, // You can specify the width here
                  ),
                ),
              )
            : null,
        padding:padding?? EdgeInsets.symmetric(vertical: 12.w, horizontal: 12.w),
        child: (leading != null)
            ? Row(
                children: [
                  leading!,
                  Gap(
                    12.w,
                  ),
                  title
                ],
              )
            : title
      ),
    );
  }
}
