import 'package:flutter/material.dart';
import '../../../../../../core/export/app_export.dart';
import '../../../../../../core/widgets/custom_list_tile.dart';

Future<void> showSelectionDialog(BuildContext context, List<String> options,
    {String? selectedValue,
      String? title,
      double? height,
      required ValueChanged<String> onSelected,
      bool showSearch = false}) async {
  TextEditingController searchController = TextEditingController();
  List<String> filteredOptions = options;

  void filterSearchResults(String query) {
    if (query.isEmpty) {
      filteredOptions = options;
    } else {
      filteredOptions = options
          .where((option) => option.toLowerCase().contains(query.toLowerCase()))
          .toList();
    }
  }

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return AlertDialog(
            backgroundColor: AppColors.background,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.w)),
            ),
            contentPadding: EdgeInsets.symmetric(vertical: 0.h, horizontal: 0.w),
            actionsPadding: EdgeInsetsDirectional.only(
                start: 12.w, end: 24.w, top: 12, bottom: 12.h),
            title: (title != null)
                ? Padding(
              padding: EdgeInsets.only(bottom: 12.h),
              child: Text(
                title,
                style:
                mediumFont500(color: AppColors.primary, fontSize: 16.w),
              ),
            )
                : null,
            content: Container(
              width: 280.w,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    if (title == null) Gap(12.h),
                    if (showSearch) ...[
                      TextField(
                        controller: searchController,
                        onChanged: (value) {
                          setState(() {
                            filterSearchResults(value);
                          });
                        },
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.search, color: AppColors.secondary),
                          hintText: 'Search',
                          hintStyle: regularFont400(
                            fontSize: 14.w,
                            color: AppColors.secondary,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: AppColors.secondary),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: AppColors.primary),
                          ),
                        ),
                      ),
                      SizedBox(height: 12.h),
                    ],
                    SizedBox(
                      height: height,
                      child: ListView.separated(
                        physics: AlwaysScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: filteredOptions.length,
                        separatorBuilder: (BuildContext context, int index) {
                          return Gap(12.h);
                        },
                        itemBuilder: (BuildContext context, int index) {
                          final bool isSelected =
                              selectedValue == filteredOptions[index];
                          final TextStyle textStyle = isSelected
                              ? mediumFont500(
                            color: AppColors.accent,
                            fontSize: 14.w,
                          )
                              : mediumFont500(
                              color: AppColors.secondary, fontSize: 14.w);
                          return CustomListTile(
                            padding: EdgeInsets.symmetric(horizontal: 12.w),
                            title: Container(
                              decoration: isSelected
                                  ? BoxDecoration(
                                color: AppColors.accent.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(8.w),
                              )
                                  : null,
                              padding: EdgeInsets.symmetric(
                                  vertical: 12.h, horizontal: 12.w),
                              child: Text(
                                filteredOptions[index],
                                style: textStyle,
                              ),
                            ),
                            onTap: () {
                              onSelected(filteredOptions[index]);
                              Navigator.pop(context);
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            actions: [
              InkWell(
                highlightColor: AppColors.background,
                splashColor: AppColors.background,
                onTap: () {
                  Navigator.pop(context);
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 6.h),
                  child: Text(
                    'Cancel',
                    style: mediumFont500(
                        color: AppColors.secondary, fontSize: 14.w),
                  ),
                ),
              ),
            ],
          );
        },
      );
    },
  );
}
