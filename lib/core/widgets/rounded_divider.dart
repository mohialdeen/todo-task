import '../export/app_export.dart';

class RoundedDivider extends StatelessWidget {
  const RoundedDivider({
    super.key,
    this.height,
    this.width,
    this.color,
  });

  final double? height;
  final double? width;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 32.w,
      height: height ?? 4.h,
      decoration: BoxDecoration(
          color: color ?? AppColors.gray,
          borderRadius: BorderRadius.circular(double.infinity)),
    );
  }
}
