import 'package:flutter/cupertino.dart';

import '../export/app_export.dart';

class CustomCupertinoSwitch extends StatelessWidget {
  CustomCupertinoSwitch({
    Key? key,
    required this.value,
    required this.active,
    this.scaleX,
    this.scaleY,
    this.onChange,
  }) : super(key: key);

  final bool value;
  final bool active;
  final double? scaleX;
  final double? scaleY;
  final ValueChanged<bool>? onChange;

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scaleX: scaleX ?? 0.82,
      scaleY: scaleY ?? 0.72,
      child: CupertinoSwitch(
        activeColor: AppColors.accent.withOpacity(0.30),
        trackColor: AppColors.gray,
        value: value,
        thumbColor: value ? AppColors.accent : AppColors.secondary,
        onChanged: active ? onChange : null,
      ),
    );
  }
}
