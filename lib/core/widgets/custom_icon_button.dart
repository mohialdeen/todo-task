//ignore: must_be_immutable
import '../export/app_export.dart';

class CustomIconButton extends StatelessWidget {
  final IconData icon;

  final void Function()? onPressed;

  final Color? color;

  double? size;

  CustomIconButton({
    super.key,
    required this.onPressed,
    required this.icon,
    this.color,
    this.size,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      highlightColor: AppColors.gray,
      splashColor: AppColors.gray,
      icon: Icon(
        icon,
        size: size ?? 24.w,
      ),
      onPressed: onPressed,
      color: color ?? AppColors.primary,
    );
  }
}
