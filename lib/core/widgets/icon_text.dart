

import '../export/app_export.dart';

class IconTextApp extends StatelessWidget {
  final IconData icon;
  final String text;
  final Color? iconColor;
  String? textTwo;
  double? iconSize;
  final bool isColum;
  final TextStyle? style;
  final TextStyle? styleTwo;
  MainAxisAlignment mainAxisAlignment;
  final bool devider;
  final bool showSplash;
  final bool createPadding;
  CrossAxisAlignment crossAxisAlignment;
  final EdgeInsets? padding;
final double? spaceBetween;
  IconTextApp(
      {Key? key,
      required this.icon,
      required this.text,
      this.style,
      this.mainAxisAlignment = MainAxisAlignment.start,
      this.crossAxisAlignment = CrossAxisAlignment.start,
      this.styleTwo,
      this.isColum = false,
      this.devider = false,
      this.createPadding = true,
      this.textTwo,
      this.iconColor,
      this.showSplash = false,
      this.iconSize,
      this.padding, this.spaceBetween});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: AppColors.gray,
      splashColor: AppColors.gray,
      onTap: showSplash ? () {} : null,
      child: Container(
        padding: createPadding
            ? padding ??
                EdgeInsetsDirectional.symmetric(horizontal: 12.w, vertical: 8.h)
            : null,
        decoration: devider
            ? BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: AppColors.gray, width: 1.w)))
            : null,
        child: Row(
          crossAxisAlignment: crossAxisAlignment,
          mainAxisAlignment: mainAxisAlignment,
          children: [
            Icon(
              icon,
              color: iconColor ?? AppColors.primary,
              size: iconSize ?? 20.w,
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(start:spaceBetween?? 8.w),
              child: textTwo != null
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          text,
                          style: style ??
                              mediumFont500(
                                  color: AppColors.primary, fontSize: 14.w),
                        ),
                        Gap(2.h),
                        if (textTwo != null)
                          Text(textTwo!,
                              style: styleTwo ??
                                  regularFont400(
                                      color: AppColors.secondary,
                                      fontSize: 14.w)),
                      ],
                    )
                  : Text(text,
                      style: style ??
                          mediumFont500(
                              color: AppColors.primary, fontSize: 14.w)),
            )
          ],
        ),
      ),
    );
  }
}
