import 'package:image_picker/image_picker.dart';

import '../export/app_export.dart';

class ProfileAvatar extends StatelessWidget {
  final String? image;
  final String userName;
  final double? size;
  final double? letterSize;
  final double? iconSize;

  final XFile? selectedImage;
  final bool noData;

  const ProfileAvatar(
      {Key? key,
      this.image,
      this.size,
      required this.userName,
      this.letterSize,
      this.iconSize,
      this.selectedImage,
      this.noData = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size ?? 40.w,
      width: size,
      child: ClipOval(
        child: selectedImage != null
            ? Image.file(
                File(selectedImage!.path),
                fit: BoxFit.cover,
                errorBuilder: (context, error, stackTrace) =>
                    _buildLetterAvatar(),
              )
            : (image != null
                ? CachedNetworkImage(
                    imageUrl: image!,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => Container(
                      decoration: ShapeDecoration(
                        shape: CircleBorder(),
                        color: AppColors.accent.withOpacity(0.25),
                      ),
                      child: Center(
                          child: CircularProgressIndicator(
                        backgroundColor: AppColors.black,
                      )),
                    ),
                    errorWidget: (context, url, error) => _buildLetterAvatar(),
                  )
                : _buildLetterAvatar()),
      ),
    );
  }

  Widget _buildLetterAvatar() {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: AppColors.accent.withOpacity(0.25),
      ),
      child: Center(
        child: noData
            ? Icon(
                AppIcons.user,
                size: iconSize ?? 20.w,
                color: AppColors.accent,
              )
            : Text(
                userName[0].toUpperCase(),
                style: boldFont700(
                  color: AppColors.accent,
                  fontSize: letterSize ?? 20.w,
                ),
              ),
      ),
    );
  }
}
