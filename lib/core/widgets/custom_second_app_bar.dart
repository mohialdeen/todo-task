

import 'package:task_manager/core/widgets/custom_icon_button.dart';
import 'package:task_manager/core/widgets/profile_avatar.dart';

import '../export/app_export.dart';

class CustomSecondAppBar extends StatelessWidget
    implements PreferredSizeWidget {
  CustomSecondAppBar(
      {super.key,
      this.title,
      this.actions,
      this.fontSize,
      this.Image,
      this.shadow = true,
      this.onLeadingTap,
      this.userName});

  String? title;
  List<Widget>? actions;
  bool shadow;
  String? userName;

  Size get preferredSize => Size.fromHeight(56.h);
  double? fontSize;
  String? Image;
  VoidCallback? onLeadingTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: shadow
          ? BoxDecoration(
              color: AppColors.background,
              boxShadow: [
                AppShadows.appBarBoxShadow(),
              ],
            )
          : null,
      child: AppBar(
        // backgroundColor: AppColors.background,
        actions: actions,
        titleSpacing: 12.w,
        leading: CustomIconButton(
          icon: CacheHelper.getData(key: AppKey.languageCode) == "ar"
              ? AppIcons.arrow_right
              : AppIcons.arrow,
          onPressed: onLeadingTap ??
              () {
                navigateBack(context: context);
              },
        ),

        title: Image != null
            ? Row(
                children: [
                  Container(
                    width: 32.w,
                    height: 32.h,
                    decoration: ShapeDecoration(
                      shape: OvalBorder(
                        side:
                            BorderSide(width: 1.w, color: Colors.grey.shade200),
                      ),
                    ),
                    child: ClipOval(
                      clipBehavior: Clip.antiAlias,
                      child: ProfileAvatar(
                        userName: userName.toString(),
                        image: Image.toString(),
                      ),
                    ),
                  ),
                  Gap(12.w),
                  Text(
                    title ?? '',
                    style: boldFont700(
                        fontSize: fontSize ?? 20.w, color: AppColors.primary),
                  ),
                ],
              )
            : Text(
                title ?? '',
                style: boldFont700(
                    fontSize: fontSize ?? 20.w, color: AppColors.primary),
              ),
      ),
    );
  }
}
