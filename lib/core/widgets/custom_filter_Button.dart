
import 'package:task_manager/core/export/app_export.dart';

class CustomFilterButton extends StatelessWidget {
  const CustomFilterButton({super.key, this.onTap});
  final VoidCallback? onTap;
  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: onTap,
      child: Container(
        padding:  EdgeInsetsDirectional.all(8.w),
        decoration: ShapeDecoration(
          color: AppColors.gray,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.w)),
        ),
        child: Icon(
          AppIcons.filter,
          color: AppColors.secondary,
        ),
      ),
    );
  }
}
