import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:task_manager/features/schedule/data/models/local_notification.dart';
import 'package:task_manager/features/schedule/data/models/task_model.dart';

class DBHelper {
  static Database? _db;
  static final int _version = 1;
  static final String _taskTable = 'tasks';
  static final String _notificationsTable = 'notifications';
  static final String _todoTable = 'todo';

  static Future<void> initDB() async {
    if (_db != null) {
      return;
    }
    try {
      String dataBasePath = await getDatabasesPath();
      String _path = join(dataBasePath, 'WBH.db');
      _db = await openDatabase(_path, version: _version, onCreate: _onCreate);
    } catch (e) {
      print(e);
      print("error ========");
    }
  }

  static _onCreate(Database db, int version) async {
    print('create a new one ');

    Batch batch = db.batch();
    batch.execute('''
    CREATE TABLE $_taskTable
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      taskName TEXT,
      description TEXT,
      date TEXT,
      startTime TEXT,
      customTask INTEGER,
      endTime TEXT,
      completed INTEGER,
      remind INTEGER,
      finishNotificationId INTEGER
    )
  ''');
    batch.execute('''
    CREATE TABLE $_notificationsTable
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      title TEXT,
      body TEXT,
      date TEXT,
      task_id INTEGER,
      FOREIGN KEY(task_id) REFERENCES $_taskTable(id)
    )
  ''');
    batch.execute('''
   CREATE TABLE $_todoTable
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      todo TEXT,
      completed INTEGER,
      userId INTEGER,
      saved INTEGER
    )
  ''');
    await batch.commit();
    print("onCreate=================");
  }

  static insertTask(TaskModel? task) async {
    print("insert fun Called");
    return await _db?.insert(_taskTable, task!.toJson()) ?? 1;
  }

  static Future<List<Map<String, dynamic>>> queryAllTasks() async {
    return await _db!.query(_taskTable);
  }

  static Future<List<Map<String, dynamic>>> queryTasks(
      DateTime selectedDate) async {
    print('queryTasks function called');
    var result = await _db!.rawQuery('SELECT * FROM $_taskTable WHERE date=?', [
      DateFormat('E, dd MMMM yyyy').format(selectedDate),
    ]);
    print('print queryTasks');
    result.forEach((row) async {
      print(row);
    });
    return result;
  }

  static insertNotification(LocalNotification? notification) async {
    return await _db?.insert(_notificationsTable, notification!.toJson()) ?? 1;
  }

  static updateEndTaskNotificationId(int taskId, int finishId) async {
    await _db!.rawUpdate('''
    UPDATE tasks
    SET finishNotificationId = ?
    WHERE id =?
    ''', [finishId, taskId]);
  }

  static queryTaskNotifications(TaskModel task) async {
    List<Map> result = await _db!.rawQuery(
        'SELECT id FROM $_notificationsTable WHERE task_id=?', [task.id]);
    print("number of notifications${result.length}");
    return result;
  }

  static deleteNotificationDB(int id) async {
    await _db!.delete(_notificationsTable, where: 'id=?', whereArgs: [id]);
  }

  static deleteTask(TaskModel task) async {
    await _db!.delete(_taskTable, where: 'id=?', whereArgs: [task.id]);
    return await _db!
        .delete(_notificationsTable, where: 'task_id=?', whereArgs: [task.id]);
  }

//  Function  in To Do Table
  static deleteTodo(id) async {
    await _db!.delete(
      _todoTable,
      where: 'id = ?',
      whereArgs: [id],
    );
    print("==============> deleted :$id");
  }

  static updateTodoCompleted(id, int completed) async {
    await _db!.update(
      _todoTable,
      {'completed': completed},
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  static insertTodo(Map<String, dynamic> todo) async {
    await _db!.insert(
      _todoTable,
      todo,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static insertMultipleTodos(List<Map<String, dynamic>> todos) async {
    Batch batch = _db!.batch();

    for (var todo in todos) {
      batch.insert(
        _todoTable,
        todo,
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }

    await batch.commit(noResult: true);
  }

  static Future<List<Map<String, dynamic>>> getAllTodos() async {
    return await _db!.query(_todoTable);
  }

  static clearToDoTable() async {
    print("============> TODO TABLE CLEARED");
    await _db!.delete(_todoTable);
  }

  static clearNotificationTable() async {
    print("============> TODO TABLE CLEARED");
    await _db!.delete(_notificationsTable);
  }

  static clearTaskTable() async {
    print("============> TODO TABLE CLEARED");
    await _db!.delete(_taskTable);
  }

  static updateSaved(int id, int newValue) async {
    return await _db!.update(
      '$_todoTable',
      {'saved': newValue},
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
