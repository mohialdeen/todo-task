class ErrorModel {
  dynamic message; // Can be String or List<String>
  String? error;
  int? status;

  ErrorModel({this.message, this.error, this.status});

  ErrorModel.fromJson(Map<String, dynamic> json) {
    if (json['message'] is String) {
      message = json['message'];
    } else if (json['message'] is List) {
      message = List<String>.from(json['message']);
    }
    error = json['error'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['error'] = this.error;
    data['status'] = this.status;
    return data;
  }

  String getMessage() {
    if (message is String) {
      return message;
    } else if (message is List) {
      return message.join(', ');
    }
    return '';
  }
}
