import 'error_model.dart';
import 'package:dio/dio.dart';
import 'error_model.dart';

class ServerException implements Exception {
  final ErrorModel errModel;

  static const String connectionTimeoutMessage =
      'Connection timeout with Server';
  static const String sendTimeoutMessage = 'Send timeout with Server';
  static const String receiveTimeoutMessage = 'Receive timeout with Server';
  static const String badCertificateMessage = 'Bad certificate';
  static const String requestCancelledMessage =
      'Request to Server was canceled';
  static const String noInternetConnectionMessage = 'No Internet Connection';
  static const String unknownErrorMessage =
      'Oops, there was an error. Please try again';
  static const String notFoundMessage =
      'Your request not found, Please try later!';
  static const String internalServerErrorMessage =
      'Internal Server error, Please try later';

  ServerException({required this.errModel});

  @override
  String toString() {
    return errModel.getMessage();
  }
}

void handleDioExceptions(DioException e) {
  ErrorModel createErrorModel(String message) {
    return ErrorModel(message: message, error: message);
  }

  ServerException createServerException(String message) {
    return ServerException(errModel: createErrorModel(message));
  }

  switch (e.type) {
    case DioExceptionType.connectionTimeout:
      throw createServerException(ServerException.connectionTimeoutMessage);
    case DioExceptionType.sendTimeout:
      throw createServerException(ServerException.sendTimeoutMessage);
    case DioExceptionType.receiveTimeout:
      throw createServerException(ServerException.receiveTimeoutMessage);
    case DioExceptionType.badCertificate:
      throw createServerException(ServerException.badCertificateMessage);
    case DioExceptionType.cancel:
      throw createServerException(ServerException.requestCancelledMessage);
    case DioExceptionType.connectionError:
      throw createServerException(ServerException.noInternetConnectionMessage);
    case DioExceptionType.unknown:
      throw createServerException(ServerException.unknownErrorMessage);
    case DioExceptionType.badResponse:

        final errorModel = ErrorModel.fromJson(e.response!.data);
        switch (e.response?.statusCode) {
          case 400:
            throw createServerException(errorModel.getMessage());
          case 401:
            throw createServerException(errorModel.getMessage());
          case 403:
            throw createServerException(errorModel.getMessage());
          case 404:
            throw createServerException(errorModel.getMessage());
          case 409:
            throw createServerException(errorModel.getMessage());
          case 422:
            throw createServerException(errorModel.getMessage());
          case 504:
            throw createServerException(errorModel.getMessage());
          default:
            throw createServerException(ServerException.unknownErrorMessage);

      }
  }
}
