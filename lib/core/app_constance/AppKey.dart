class AppKey{
  static String languageCode="LANGUAGE_CODE";
  static String token = "token";
  static String refreshToken = "refreshToken";
  static String bearer = "Bearer";
  static String data = "data";
  static String userId = "id";
  static String username = "username";
  static String email = "email";
  static String firstName = "firstName";
  static String lastName = "lastName";
  static String gender = "gender";
  static String image = "image";
}