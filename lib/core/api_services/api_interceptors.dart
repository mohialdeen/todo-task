import 'package:task_manager/core/api_services/end_ponits.dart';

import '../export/app_export.dart';

class ApiInterceptor extends Interceptor {
  ApiInterceptor({required this.dio});

  final Dio dio;

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
  //  options.headers[HttpHeaders.acceptHeader] = ContentType.json;
    options.headers[HttpHeaders.contentTypeHeader]='application/json';
    options.headers[HttpHeaders.authorizationHeader] = CacheHelper.getData(
                key: AppKey.token) !=
            null
        ? "${AppKey.bearer} ${CacheHelper.getData(key: AppKey.token)}"
        : null;
    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    debugPrint(
        'RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}');
    super.onResponse(response, handler);
  }

  @override
  void onError(DioException error, ErrorInterceptorHandler handler) async {
    if (error.response?.statusCode == 401) {
      final newToken = await refreshToken();
      if (newToken != null) {
        dio.options.headers[AppKey.token] =
            "${AppKey.token} $newToken";
        return handler.resolve(await dio.fetch(error.requestOptions));
      }
    }
    return handler.next(error);
  }

  Future<String?> refreshToken() async {
// create refresh Token method to get new token and new refresh token
    try {
      final String refreshToken =
          CacheHelper.getData(key: AppKey.refreshToken);
      final response = await dio.post(EndPoint.refreshToken,
          data: {AppKey.refreshToken: refreshToken});
      final newToken =
          response.data[AppKey.data][AppKey.token];
      final newRefreshToken =
          response.data[AppKey.data][AppKey.refreshToken];
      CacheHelper.saveData(key: AppKey.token, value: newToken);
      CacheHelper.saveData(key: AppKey.refreshToken, value: newRefreshToken);
      return newToken;
    } catch (exception) {
      //TODO: clear cacheData and navigate to log in Screen
    }
    return null;
  }
}
