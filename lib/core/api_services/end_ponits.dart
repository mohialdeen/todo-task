class EndPoint {
  static String baseUrl = 'https://dummyjson.com/';
  static String refreshToken = "auth/refresh";
  static String logIn = "auth/login";
  static String getTodo = "todos";
  static String addTodo = "todos/add";

  static String deleteTodo({required todoId}) => "todos/$todoId";

  static String updateTodo({required todoId}) => "todos/$todoId";
}
