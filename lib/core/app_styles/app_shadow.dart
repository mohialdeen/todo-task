
import '../export/app_export.dart';

class AppShadows {
  static  BoxShadow appBarBoxShadow() {
    return BoxShadow(
      color: WBHColors.black.withOpacity(0.08),
      blurRadius: 14,
      offset: Offset(0, 1),
    );
  }
  static  BoxShadow sectionShadow() {
    return BoxShadow(
      color: WBHColors.black.withOpacity(0.15),
      blurRadius: 7.2,
      offset: Offset(0, 2),
    );
  }
  static  BoxShadow cardShadow() {
    return BoxShadow(
      color: WBHColors.black.withOpacity(0.06),
      blurRadius: 9,
      offset: Offset(0, 2),
    );
  }
}
