import 'package:flutter/material.dart';
import 'package:task_manager/core/app_constance/AppKey.dart';
import 'package:task_manager/core/cache_helper/cache_helper.dart';

class SchnebelSans {
  static const regular = 'SchnebelSansPro-Regular';
  static const medium = 'SchnebelSansPro-Medium';
  static const bold = 'SchnebelSansPro-Bold';
}

class NotoKufiArabic {
  static const regular = 'NotoKufiArabic-Regular';
  static const medium = 'NotoKufiArabic-Medium';
  static const bold = 'NotoKufiArabic-Bold';
}

bool isEnglish = CacheHelper.getData(key: AppKey.languageCode)=="en"?true:false;

TextStyle getTextStyle(
    {fontFamily, double? fontSize, double? height, Color? color}) {
  return TextStyle(
      fontFamily: fontFamily, fontSize: fontSize, height: height, color: color);
}

TextStyle regularFont400(
    {double? fontSize = 14, height = 1.4, required color}) {
  return getTextStyle(
      fontFamily: isEnglish ? SchnebelSans.regular : NotoKufiArabic.regular,
      fontSize: fontSize,
      height: 1.4,
      color: color);
}

TextStyle mediumFont500({double? fontSize = 14, height = 1.4, required color}) {
  return getTextStyle(
      fontFamily: isEnglish ? SchnebelSans.medium : NotoKufiArabic.medium,
      fontSize: fontSize,
      height: height,
      color: color);
}

TextStyle boldFont700({double? fontSize = 14, height = 1.6, required color}) {
  return getTextStyle(
      fontFamily: isEnglish ? SchnebelSans.bold : NotoKufiArabic.bold,
      fontSize: fontSize,
      height: height,
      color: color);
}
