// ignore_for_file: prefer_const_constructors

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:task_manager/features/schedule/data/models/reminder_model.dart';
import 'package:task_manager/features/schedule/data/models/task_model.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tz;


class NotifyHelper {
  static FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();

  static initializeNotification() async {
    print("init NotifyHelper");
    _configureLocalTimeZone();
    final DarwinInitializationSettings initializationSettingsIOS =
    DarwinInitializationSettings(
      requestSoundPermission: false,
      requestBadgePermission: false,
      requestAlertPermission: false,
    );

    final AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings("@mipmap/ic_launcher");
    final InitializationSettings initializationSettings =
    InitializationSettings(
      iOS: initializationSettingsIOS,
      android: initializationSettingsAndroid,
    );
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onDidReceiveNotificationResponse: (payload) {});
  }

  static displayNotification(
      {required String title, required String body}) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        '123', 'theme changed',
        channelDescription: 'dark or light mode',
        importance: Importance.max,
        priority: Priority.high);

    var iOSPlatformChannelSpecifics = new DarwinNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0, //unique id for each notification
      title,
      body,
      platformChannelSpecifics,
      payload: title,
    );
  }

  static Future<void> _configureLocalTimeZone() async {
    tz.initializeTimeZones();
    final String timeZone = await FlutterNativeTimezone.getLocalTimezone();
    tz.setLocalLocation(tz.getLocation(timeZone));
  }

  static scheduledNotification(
      {
        required int id,
        required TaskModel task,
        required String type,
        required DateTime time
      }) async {
    String taskJsonString = task.toJsonString();

    await flutterLocalNotificationsPlugin.zonedSchedule(
        id,
        getNotificationTitle(type, task.taskName!, task.remind),
        getNotificationBody(type, task.taskName!, task.description!),
        _convertTime(time),
        const NotificationDetails(
          android: AndroidNotificationDetails('channel id', 'channel name',
              channelDescription: 'channel description'), // iOS details
          iOS: DarwinNotificationDetails(
            sound: 'default.wav',
            presentAlert: true,
            presentBadge: true,
            presentSound:
            true, // Play a sound when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
          ),
        ),
        //androidScheduleMode: AndroidScheduleMode.alarmClock,
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.absoluteTime,
        payload: taskJsonString);
  }



  static String getNotificationTitle(String type, String title, int? remind) {
    switch (type) {
      case 'startTime':
        return title;
      case 'endTime':
        return 'Have you finished your task?';
      case 'reminder':
        return ReminderModel.ReminderList()[int.parse(remind.toString())]
            .titleEn;
      default:
        return title;
    }
  }

  static String getNotificationBody(String type, String title, String body) {
    switch (type) {
      case 'startTime':
        return body;
      case 'endTime':
        return title;
      case 'reminder':
        return title;
      default:
        return title;
    }
  }

  static tz.TZDateTime _convertTime( DateTime date) {
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduleDate =
    tz.TZDateTime(tz.local, date.year, date.month, date.day,date.hour, date.minute);
    if (scheduleDate.isBefore(now)) {
      scheduleDate = scheduleDate.add(const Duration(days: 1));
    }
    return scheduleDate;
  }

  void requestIOSPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );
  }
  static Future<void> cancelNotifications(int id) async {
    await flutterLocalNotificationsPlugin.cancel(id);
  }

}
