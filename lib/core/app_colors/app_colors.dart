import '../export/app_export.dart';

class WBHColors {
  static const accent = Color(0xff437A99);
  static const white = Color(0xffF4F4F4);
  static const black = Color(0xff282A2C);
  static const red = Color(0xffB73541);
  static const yellow = Color(0xffFFCB45);
  static const green = Color(0xff799E6E);

  static const backgroundLight = Color(0xffF5F5F5);
  static const backgroundDark = Color(0xff282A2C);
  static const primaryLight = Color(0xff282A2C);
  static const primaryDark = Color(0xffF4F4F4);
  static const secondaryLight = Color(0xff939393);
  static const secondaryDark = Color(0xff939393);
  static const grayLight = Color(0xffE9E9E9);
  static const lightAccent = Color(0xffD1DCE3);
  static const darkAccent = Color(0xff2D3A42);
  static const lightRed = Color(0xffEDCFD2);
  static const darkRed = Color(0xff492D31);
  static const grayDark = Color(0xff383A3E);
  static const baseLight = Color(0xffFCFCFC);
  static const baseDark = Color(0xff2D2F31);
}

MaterialColor buildMaterialColor(Color color) {
  List strengths = <double>[.05];
  Map<int, Color> swatch = {};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  for (var strength in strengths) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  }
  return MaterialColor(color.value, swatch);
}
class AppColors with ChangeNotifier{
  static bool isLightTheme = CacheHelper.getData(key: "isLightTheme")??true;

  static const Color accent = WBHColors.accent;
  static const Color red = WBHColors.red;
  static const Color green = WBHColors.green;
  static const Color yellow = WBHColors.yellow;
  static const Color white = WBHColors.white;
  static const Color black = WBHColors.black;

  static  Color get background => isLightTheme ? WBHColors.backgroundLight : WBHColors.backgroundDark;
  static Color get primary => isLightTheme ? WBHColors.primaryLight : WBHColors.primaryDark;
  static Color get secondary => isLightTheme ? WBHColors.secondaryLight : WBHColors.secondaryDark;
  static Color get gray => isLightTheme ? WBHColors.grayLight : WBHColors.grayDark;
  static Color get base => isLightTheme ? WBHColors.baseLight : WBHColors.baseDark;
  static Color get secondaryAccent => isLightTheme ? WBHColors.lightAccent : WBHColors.darkAccent;
  static Color get secondaryRed => isLightTheme ? WBHColors.lightRed : WBHColors.darkRed;
  static Color get accentFontColor => isLightTheme ? WBHColors.accent : WBHColors.white;
  static Color get redFontColor => isLightTheme ? WBHColors.red : WBHColors.white;
}
