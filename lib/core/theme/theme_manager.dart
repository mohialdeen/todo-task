import '../export/app_export.dart';

class ThemeManager with ChangeNotifier {
  ThemeMode _themeMode = CacheHelper.getData(key: "isLightTheme")?ThemeMode.light:ThemeMode.dark;

  get themeMode => _themeMode;
  toggleTheme(bool isDark)
  {
    _themeMode=isDark?ThemeMode.dark:ThemeMode.light;
    notifyListeners();
  }

}
