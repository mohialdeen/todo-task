import 'package:flutter/material.dart';
import 'package:task_manager/core/app_colors/app_colors.dart';

ThemeData lightTheme = ThemeData(
  colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accent),
  scaffoldBackgroundColor: WBHColors.backgroundLight,
  appBarTheme: AppBarTheme(backgroundColor: WBHColors.backgroundLight),
  useMaterial3: true,
);

ThemeData darkTheme = ThemeData(
  colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accent),
  scaffoldBackgroundColor: WBHColors.backgroundDark,
  appBarTheme: AppBarTheme(backgroundColor: WBHColors.backgroundDark),
  useMaterial3: true,
);
