import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import '../app_colors/app_colors.dart';

void loadingAnimation(context) {
  showDialog(
    barrierColor:Colors.transparent,
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return Center(

            child: LoadingAnimationWidget.hexagonDots(

                color: AppColors.primary, size: 50));

      });
}

void secondLoadingAnimation(
  context,
) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return Center(
            child: LoadingAnimationWidget.discreteCircle(
                secondRingColor: AppColors.primary,
                color: AppColors.accent,
                size: 50));
      });
}
