import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';


navigateAndRemoveUntil({
  required BuildContext context,
  required Widget screen,
}) {
  Navigator.of(context).pushAndRemoveUntil(
    MaterialPageRoute(builder: (context) => screen),
    (route) => false,
  );
}

navigateBack({
  required BuildContext context,
}){
  Navigator.of(context).pop();
}

Future<dynamic> navigateTo({required BuildContext context, required Widget screen}) {
  return Navigator.push(
      context,
      PageTransition(
          type: PageTransitionType.rightToLeftWithFade, child: screen));
}
Future<dynamic> navigateAndRemoveTo({
  required BuildContext context,
  required Widget screen,
}) {
  return Navigator.pushAndRemoveUntil(
    context,
    PageTransition(
      type: PageTransitionType.rightToLeftWithFade,
      child: screen,
    ),
        (Route<dynamic> route) => false, // This will remove all previous routes
  );
}