
import '../export/app_export.dart';

customSnackBar({
  required BuildContext context,
  required String label,
  required Color color ,
  required Color fontColor,
}) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(
        label,
        style: regularFont400(color: fontColor,fontSize: 12.w),
      ),
      backgroundColor: color,
      padding:EdgeInsets.symmetric(vertical: 12.h, horizontal: 12.w) ,
      behavior: SnackBarBehavior.floating,
    ),
  );
}
