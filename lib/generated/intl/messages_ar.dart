// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ar locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ar';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Date": MessageLookupByLibrary.simpleMessage("التاريخ"),
        "apply": MessageLookupByLibrary.simpleMessage("تطبيق"),
        "arabic": MessageLookupByLibrary.simpleMessage("العربية"),
        "cancel": MessageLookupByLibrary.simpleMessage("إلغاء"),
        "completed": MessageLookupByLibrary.simpleMessage("مكتملة"),
        "completed_successfully":
            MessageLookupByLibrary.simpleMessage("تمت العملية بنجاح"),
        "create": MessageLookupByLibrary.simpleMessage("إنشاء"),
        "create_task": MessageLookupByLibrary.simpleMessage("إنشاء مهمة"),
        "dark_theme": MessageLookupByLibrary.simpleMessage("السمة الداكنة"),
        "date": MessageLookupByLibrary.simpleMessage("التاريخ"),
        "delete": MessageLookupByLibrary.simpleMessage("حذف"),
        "delete_task": MessageLookupByLibrary.simpleMessage("حذف المهمة"),
        "description": MessageLookupByLibrary.simpleMessage("الوصف"),
        "edit": MessageLookupByLibrary.simpleMessage("تحرير"),
        "edit_task": MessageLookupByLibrary.simpleMessage("تحرير المهمة"),
        "email": MessageLookupByLibrary.simpleMessage("البريد الإلكتروني"),
        "end_time": MessageLookupByLibrary.simpleMessage("وقت الانتهاء"),
        "english": MessageLookupByLibrary.simpleMessage("الإنجليزية"),
        "enter_password":
            MessageLookupByLibrary.simpleMessage("أدخل كلمة المرور"),
        "enter_the_description_of_the_task":
            MessageLookupByLibrary.simpleMessage("أدخل وصف المهمة"),
        "enter_the_name_of_the_task":
            MessageLookupByLibrary.simpleMessage("أدخل اسم المهمة"),
        "enter_user_name":
            MessageLookupByLibrary.simpleMessage("أدخل اسم المستخدم"),
        "first_name": MessageLookupByLibrary.simpleMessage("الاسم الأول"),
        "from": MessageLookupByLibrary.simpleMessage("من"),
        "full_name": MessageLookupByLibrary.simpleMessage("الاسم الكامل"),
        "gender": MessageLookupByLibrary.simpleMessage("الجنس"),
        "language": MessageLookupByLibrary.simpleMessage("اللغة"),
        "last_name": MessageLookupByLibrary.simpleMessage("اسم العائلة"),
        "light_theme": MessageLookupByLibrary.simpleMessage("السمة الفاتحة"),
        "loa_more": MessageLookupByLibrary.simpleMessage("حرر للتحميل المزيد"),
        "load_failed": MessageLookupByLibrary.simpleMessage(
            "فشل التحميل! اضغط لإعادة المحاولة!"),
        "log_in": MessageLookupByLibrary.simpleMessage("تسجيل الدخول"),
        "log_in_and_start_managing_your_tasks":
            MessageLookupByLibrary.simpleMessage(
                "سجل الدخول وابدأ في إدارة مهامك"),
        "log_out": MessageLookupByLibrary.simpleMessage("تسجيل الخروج"),
        "month": MessageLookupByLibrary.simpleMessage("شهر"),
        "new_task": MessageLookupByLibrary.simpleMessage("مهمة جديدة"),
        "new_toDo": MessageLookupByLibrary.simpleMessage("مهمة جديدة"),
        "no_more_tasks":
            MessageLookupByLibrary.simpleMessage("لا توجد مهام أخرى"),
        "no_tasks_to_show": MessageLookupByLibrary.simpleMessage(
            "لا توجد مهام لعرضها الآن. انقر على زر \'+\' أدناه لإضافة مهمة جديدة والبقاء منظمًا!"),
        "not_completed": MessageLookupByLibrary.simpleMessage("غير مكتملة"),
        "password": MessageLookupByLibrary.simpleMessage("كلمة المرور"),
        "please_enter_what_you_want_to_do":
            MessageLookupByLibrary.simpleMessage("يرجى إدخال ما تريد فعله"),
        "profile": MessageLookupByLibrary.simpleMessage("الملف الشخصي"),
        "pull_up":
            MessageLookupByLibrary.simpleMessage("اسحب للأعلى لتحميل المزيد"),
        "reminder": MessageLookupByLibrary.simpleMessage("تذكير"),
        "save": MessageLookupByLibrary.simpleMessage("حفظ !"),
        "saved": MessageLookupByLibrary.simpleMessage("محفوظ"),
        "schedule": MessageLookupByLibrary.simpleMessage("الجدول الزمني"),
        "search": MessageLookupByLibrary.simpleMessage("بحث"),
        "settings": MessageLookupByLibrary.simpleMessage("الإعدادات"),
        "show_all": MessageLookupByLibrary.simpleMessage("عرض الكل"),
        "something_went_wrong_try_again":
            MessageLookupByLibrary.simpleMessage("حدث خطأ ما. حاول مرة أخرى"),
        "start_time": MessageLookupByLibrary.simpleMessage("وقت البدء"),
        "task": MessageLookupByLibrary.simpleMessage("مهمة"),
        "task_name": MessageLookupByLibrary.simpleMessage("اسم المهمة"),
        "theme": MessageLookupByLibrary.simpleMessage("السمة"),
        "to": MessageLookupByLibrary.simpleMessage("إلى"),
        "todo": MessageLookupByLibrary.simpleMessage("مهمة"),
        "user_name": MessageLookupByLibrary.simpleMessage("اسم المستخدم"),
        "view_profile":
            MessageLookupByLibrary.simpleMessage("عرض الملف الشخصي"),
        "week": MessageLookupByLibrary.simpleMessage("أسبوع"),
        "welcomeBack": MessageLookupByLibrary.simpleMessage("مرحبًا بعودتك!"),
        "what_you_want_to_do":
            MessageLookupByLibrary.simpleMessage("ماذا تريد أن تفعل")
      };
}
