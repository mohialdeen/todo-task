// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Date": MessageLookupByLibrary.simpleMessage("date"),
        "apply": MessageLookupByLibrary.simpleMessage("apply"),
        "arabic": MessageLookupByLibrary.simpleMessage("Arabic"),
        "cancel": MessageLookupByLibrary.simpleMessage("cancel"),
        "completed": MessageLookupByLibrary.simpleMessage("Completed"),
        "completed_successfully":
            MessageLookupByLibrary.simpleMessage("completed successfully"),
        "create": MessageLookupByLibrary.simpleMessage("Create"),
        "create_task": MessageLookupByLibrary.simpleMessage("Create Task"),
        "dark_theme": MessageLookupByLibrary.simpleMessage("Dark theme"),
        "date": MessageLookupByLibrary.simpleMessage("Date"),
        "delete": MessageLookupByLibrary.simpleMessage("delete"),
        "delete_task": MessageLookupByLibrary.simpleMessage("Delete task"),
        "description": MessageLookupByLibrary.simpleMessage("Description"),
        "edit": MessageLookupByLibrary.simpleMessage("edit"),
        "edit_task": MessageLookupByLibrary.simpleMessage("Edit task"),
        "email": MessageLookupByLibrary.simpleMessage("email"),
        "end_time": MessageLookupByLibrary.simpleMessage("End time"),
        "english": MessageLookupByLibrary.simpleMessage("English"),
        "enter_password":
            MessageLookupByLibrary.simpleMessage("enter password"),
        "enter_the_description_of_the_task":
            MessageLookupByLibrary.simpleMessage(
                "Enter the description of the task"),
        "enter_the_name_of_the_task":
            MessageLookupByLibrary.simpleMessage("Enter the name of the task"),
        "enter_user_name":
            MessageLookupByLibrary.simpleMessage("enter user name"),
        "first_name": MessageLookupByLibrary.simpleMessage("First name"),
        "from": MessageLookupByLibrary.simpleMessage("From"),
        "full_name": MessageLookupByLibrary.simpleMessage("Full name"),
        "gender": MessageLookupByLibrary.simpleMessage("gender"),
        "language": MessageLookupByLibrary.simpleMessage("Language"),
        "last_name": MessageLookupByLibrary.simpleMessage("last name"),
        "light_theme": MessageLookupByLibrary.simpleMessage("Light theme"),
        "loa_more":
            MessageLookupByLibrary.simpleMessage("Release to load more"),
        "load_failed":
            MessageLookupByLibrary.simpleMessage("Load Failed! Click retry!"),
        "log_in": MessageLookupByLibrary.simpleMessage("Log in"),
        "log_in_and_start_managing_your_tasks":
            MessageLookupByLibrary.simpleMessage(
                "Log in and start managing your tasks"),
        "log_out": MessageLookupByLibrary.simpleMessage("Log Out"),
        "month": MessageLookupByLibrary.simpleMessage("Month"),
        "new_task": MessageLookupByLibrary.simpleMessage("New task"),
        "new_toDo": MessageLookupByLibrary.simpleMessage("New Todo"),
        "no_more_tasks": MessageLookupByLibrary.simpleMessage("No more tasks"),
        "no_tasks_to_show": MessageLookupByLibrary.simpleMessage(
            "No tasks to show right now. Tap the \\\'+\\\' button below to add a new task and stay organized!\n"),
        "not_completed": MessageLookupByLibrary.simpleMessage("Not Completed"),
        "password": MessageLookupByLibrary.simpleMessage("password"),
        "please_enter_what_you_want_to_do":
            MessageLookupByLibrary.simpleMessage(
                "please enter what you want to do"),
        "profile": MessageLookupByLibrary.simpleMessage("profile"),
        "pull_up": MessageLookupByLibrary.simpleMessage("Pull up to load more"),
        "reminder": MessageLookupByLibrary.simpleMessage("Reminder"),
        "save": MessageLookupByLibrary.simpleMessage("save !"),
        "saved": MessageLookupByLibrary.simpleMessage("Saved"),
        "schedule": MessageLookupByLibrary.simpleMessage("schedule"),
        "search": MessageLookupByLibrary.simpleMessage("Search"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "show_all": MessageLookupByLibrary.simpleMessage("show all"),
        "something_went_wrong_try_again": MessageLookupByLibrary.simpleMessage(
            "Something went wrong. Try again"),
        "start_time": MessageLookupByLibrary.simpleMessage("Start time"),
        "task": MessageLookupByLibrary.simpleMessage("Task"),
        "task_name": MessageLookupByLibrary.simpleMessage("Task name"),
        "theme": MessageLookupByLibrary.simpleMessage("Theme"),
        "to": MessageLookupByLibrary.simpleMessage("To"),
        "todo": MessageLookupByLibrary.simpleMessage("Todo"),
        "user_name": MessageLookupByLibrary.simpleMessage("User Name"),
        "view_profile": MessageLookupByLibrary.simpleMessage("View Profile"),
        "week": MessageLookupByLibrary.simpleMessage("Week"),
        "welcomeBack": MessageLookupByLibrary.simpleMessage("Welcome back!"),
        "what_you_want_to_do":
            MessageLookupByLibrary.simpleMessage("what you want to do")
      };
}
