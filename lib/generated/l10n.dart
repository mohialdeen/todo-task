// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Welcome back!`
  String get welcomeBack {
    return Intl.message(
      'Welcome back!',
      name: 'welcomeBack',
      desc: '',
      args: [],
    );
  }

  /// `Log in`
  String get log_in {
    return Intl.message(
      'Log in',
      name: 'log_in',
      desc: '',
      args: [],
    );
  }

  /// `User Name`
  String get user_name {
    return Intl.message(
      'User Name',
      name: 'user_name',
      desc: '',
      args: [],
    );
  }

  /// `password`
  String get password {
    return Intl.message(
      'password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `enter user name`
  String get enter_user_name {
    return Intl.message(
      'enter user name',
      name: 'enter_user_name',
      desc: '',
      args: [],
    );
  }

  /// `enter password`
  String get enter_password {
    return Intl.message(
      'enter password',
      name: 'enter_password',
      desc: '',
      args: [],
    );
  }

  /// `Log in and start managing your tasks`
  String get log_in_and_start_managing_your_tasks {
    return Intl.message(
      'Log in and start managing your tasks',
      name: 'log_in_and_start_managing_your_tasks',
      desc: '',
      args: [],
    );
  }

  /// `View Profile`
  String get view_profile {
    return Intl.message(
      'View Profile',
      name: 'view_profile',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settings {
    return Intl.message(
      'Settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Log Out`
  String get log_out {
    return Intl.message(
      'Log Out',
      name: 'log_out',
      desc: '',
      args: [],
    );
  }

  /// `schedule`
  String get schedule {
    return Intl.message(
      'schedule',
      name: 'schedule',
      desc: '',
      args: [],
    );
  }

  /// `show all`
  String get show_all {
    return Intl.message(
      'show all',
      name: 'show_all',
      desc: '',
      args: [],
    );
  }

  /// `Month`
  String get month {
    return Intl.message(
      'Month',
      name: 'month',
      desc: '',
      args: [],
    );
  }

  /// `Week`
  String get week {
    return Intl.message(
      'Week',
      name: 'week',
      desc: '',
      args: [],
    );
  }

  /// `Description`
  String get description {
    return Intl.message(
      'Description',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `Task`
  String get task {
    return Intl.message(
      'Task',
      name: 'task',
      desc: '',
      args: [],
    );
  }

  /// `New task`
  String get new_task {
    return Intl.message(
      'New task',
      name: 'new_task',
      desc: '',
      args: [],
    );
  }

  /// `Task name`
  String get task_name {
    return Intl.message(
      'Task name',
      name: 'task_name',
      desc: '',
      args: [],
    );
  }

  /// `date`
  String get Date {
    return Intl.message(
      'date',
      name: 'Date',
      desc: '',
      args: [],
    );
  }

  /// `From`
  String get from {
    return Intl.message(
      'From',
      name: 'from',
      desc: '',
      args: [],
    );
  }

  /// `To`
  String get to {
    return Intl.message(
      'To',
      name: 'to',
      desc: '',
      args: [],
    );
  }

  /// `Reminder`
  String get reminder {
    return Intl.message(
      'Reminder',
      name: 'reminder',
      desc: '',
      args: [],
    );
  }

  /// `Enter the description of the task`
  String get enter_the_description_of_the_task {
    return Intl.message(
      'Enter the description of the task',
      name: 'enter_the_description_of_the_task',
      desc: '',
      args: [],
    );
  }

  /// `Start time`
  String get start_time {
    return Intl.message(
      'Start time',
      name: 'start_time',
      desc: '',
      args: [],
    );
  }

  /// `End time`
  String get end_time {
    return Intl.message(
      'End time',
      name: 'end_time',
      desc: '',
      args: [],
    );
  }

  /// `Enter the name of the task`
  String get enter_the_name_of_the_task {
    return Intl.message(
      'Enter the name of the task',
      name: 'enter_the_name_of_the_task',
      desc: '',
      args: [],
    );
  }

  /// `Date`
  String get date {
    return Intl.message(
      'Date',
      name: 'date',
      desc: '',
      args: [],
    );
  }

  /// `Create Task`
  String get create_task {
    return Intl.message(
      'Create Task',
      name: 'create_task',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search {
    return Intl.message(
      'Search',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `profile`
  String get profile {
    return Intl.message(
      'profile',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `last name`
  String get last_name {
    return Intl.message(
      'last name',
      name: 'last_name',
      desc: '',
      args: [],
    );
  }

  /// `First name`
  String get first_name {
    return Intl.message(
      'First name',
      name: 'first_name',
      desc: '',
      args: [],
    );
  }

  /// `email`
  String get email {
    return Intl.message(
      'email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `gender`
  String get gender {
    return Intl.message(
      'gender',
      name: 'gender',
      desc: '',
      args: [],
    );
  }

  /// `Full name`
  String get full_name {
    return Intl.message(
      'Full name',
      name: 'full_name',
      desc: '',
      args: [],
    );
  }

  /// `Theme`
  String get theme {
    return Intl.message(
      'Theme',
      name: 'theme',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get language {
    return Intl.message(
      'Language',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `Arabic`
  String get arabic {
    return Intl.message(
      'Arabic',
      name: 'arabic',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get english {
    return Intl.message(
      'English',
      name: 'english',
      desc: '',
      args: [],
    );
  }

  /// `cancel`
  String get cancel {
    return Intl.message(
      'cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `apply`
  String get apply {
    return Intl.message(
      'apply',
      name: 'apply',
      desc: '',
      args: [],
    );
  }

  /// `Light theme`
  String get light_theme {
    return Intl.message(
      'Light theme',
      name: 'light_theme',
      desc: '',
      args: [],
    );
  }

  /// `Dark theme`
  String get dark_theme {
    return Intl.message(
      'Dark theme',
      name: 'dark_theme',
      desc: '',
      args: [],
    );
  }

  /// `Pull up to load more`
  String get pull_up {
    return Intl.message(
      'Pull up to load more',
      name: 'pull_up',
      desc: '',
      args: [],
    );
  }

  /// `Load Failed! Click retry!`
  String get load_failed {
    return Intl.message(
      'Load Failed! Click retry!',
      name: 'load_failed',
      desc: '',
      args: [],
    );
  }

  /// `Release to load more`
  String get loa_more {
    return Intl.message(
      'Release to load more',
      name: 'loa_more',
      desc: '',
      args: [],
    );
  }

  /// `No more tasks`
  String get no_more_tasks {
    return Intl.message(
      'No more tasks',
      name: 'no_more_tasks',
      desc: '',
      args: [],
    );
  }

  /// `Edit task`
  String get edit_task {
    return Intl.message(
      'Edit task',
      name: 'edit_task',
      desc: '',
      args: [],
    );
  }

  /// `Delete task`
  String get delete_task {
    return Intl.message(
      'Delete task',
      name: 'delete_task',
      desc: '',
      args: [],
    );
  }

  /// `Completed`
  String get completed {
    return Intl.message(
      'Completed',
      name: 'completed',
      desc: '',
      args: [],
    );
  }

  /// `Not Completed`
  String get not_completed {
    return Intl.message(
      'Not Completed',
      name: 'not_completed',
      desc: '',
      args: [],
    );
  }

  /// `edit`
  String get edit {
    return Intl.message(
      'edit',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `delete`
  String get delete {
    return Intl.message(
      'delete',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `New Todo`
  String get new_toDo {
    return Intl.message(
      'New Todo',
      name: 'new_toDo',
      desc: '',
      args: [],
    );
  }

  /// `what you want to do`
  String get what_you_want_to_do {
    return Intl.message(
      'what you want to do',
      name: 'what_you_want_to_do',
      desc: '',
      args: [],
    );
  }

  /// `please enter what you want to do`
  String get please_enter_what_you_want_to_do {
    return Intl.message(
      'please enter what you want to do',
      name: 'please_enter_what_you_want_to_do',
      desc: '',
      args: [],
    );
  }

  /// `Todo`
  String get todo {
    return Intl.message(
      'Todo',
      name: 'todo',
      desc: '',
      args: [],
    );
  }

  /// `Create`
  String get create {
    return Intl.message(
      'Create',
      name: 'create',
      desc: '',
      args: [],
    );
  }

  /// `Something went wrong. Try again`
  String get something_went_wrong_try_again {
    return Intl.message(
      'Something went wrong. Try again',
      name: 'something_went_wrong_try_again',
      desc: '',
      args: [],
    );
  }

  /// `completed successfully`
  String get completed_successfully {
    return Intl.message(
      'completed successfully',
      name: 'completed_successfully',
      desc: '',
      args: [],
    );
  }

  /// `save !`
  String get save {
    return Intl.message(
      'save !',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Saved`
  String get saved {
    return Intl.message(
      'Saved',
      name: 'saved',
      desc: '',
      args: [],
    );
  }

  /// `No tasks to show right now. Tap the \'+\' button below to add a new task and stay organized!\n`
  String get no_tasks_to_show {
    return Intl.message(
      'No tasks to show right now. Tap the \\\'+\\\' button below to add a new task and stay organized!\n',
      name: 'no_tasks_to_show',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ar'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
