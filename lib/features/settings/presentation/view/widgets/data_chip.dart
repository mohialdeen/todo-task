import '../../../../../core/export/app_export.dart';

class DataChip extends StatelessWidget {
  const DataChip({
    required this.title, required this.content, this.icon, this.color,
    super.key,
  });

  final String title;
  final String content;
  final IconData? icon;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery
          .sizeOf(context)
          .width,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: AppColors.gray, // You can specify the color here
            width: 1.0, // You can specify the width here
          ),
        ),
      ),
      padding: EdgeInsets.symmetric(vertical: 16.h, horizontal: 12.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          (icon != null) ? Row(
            children: [
              Icon(
                icon,
                size: 20.w,
                color: color ?? AppColors.secondary,
              ),
               SizedBox(
                width: 8.w,
              ),
              Text(
                title,
                style: mediumFont500(
                    color: color ?? AppColors.secondary, fontSize: 14.w),
              )
            ],
          ) : Text(
            title,
            style: mediumFont500(
                color: color ?? AppColors.secondary, fontSize: 14.w),
          ),
          SizedBox(height: 8.h,),
          Text(
            content,
            style: mediumFont500(
                color: color ?? AppColors.secondary, fontSize: 14.w),
          ),
        ],
      ),
    );
  }
}
