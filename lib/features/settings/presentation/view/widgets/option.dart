import '../../../../../core/app_colors/app_colors.dart';
import '../../../../../core/app_styles/app_icons_icons.dart';
import '../../../../../core/export/app_export.dart';

class Option extends StatelessWidget {
  const Option({
    required this.text,
    this.icon,
    this.onTap,
    this.isSelected = false,
    super.key,
  });

  final String text;
  final IconData? icon;
  final Function? onTap;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Container(

      width: 264.w,
      child: ListTile(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.w))),
        // minLeadingWidth: 12.w,
        minVerticalPadding: 12.w,
        horizontalTitleGap: 8.w,
        splashColor: Colors.transparent,
        selected: isSelected,
        selectedTileColor:isSelected? AppColors.accent.withOpacity(0.15):null,
        enabled: !isSelected,
        onLongPress: () {},

        contentPadding: EdgeInsets.symmetric(horizontal: 12.w),

        leading: (icon != null)
            ? Icon(
          icon,
          color: isSelected ? AppColors.accent : AppColors.primary,
          size: 24.w,
        )
            : null,
        trailing: isSelected
            ? Icon(
          AppIcons.radio_selected,
          size: 20.w,
          color: AppColors.accent,
        )
            : Icon(
          AppIcons.radio_unselceted,
          size: 20.w,
          color: AppColors.primary,
        ),
        title: Text(
          text,
          style: mediumFont500(
              color: isSelected ? AppColors.accent : AppColors.primary,
              fontSize: 14.w),
        ),
        onTap: () {
          if (onTap != null) {
            onTap!();
          }
        },
      ),
    );
  }
}