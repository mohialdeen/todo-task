import '../../../../../core/app_colors/app_colors.dart';
import '../../../../../core/export/app_export.dart';

class SettingsOption extends StatelessWidget {
  const SettingsOption(
      {super.key, required this.text, this.icon, this.color, this.onTap});

  final String text;
  final IconData? icon;
  final Color? color;
  final Function? onTap;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap!();
        }
      },
      splashColor: AppColors.gray,
      highlightColor: AppColors.gray,
      child: Container(
        width: MediaQuery.sizeOf(context).width,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: AppColors.gray, // You can specify the color here
              width: 1.0, // You can specify the width here
            ),
          ),
        ),
        padding: EdgeInsets.symmetric(vertical: 16.h, horizontal: 12.w),
        child: (icon != null)
            ? Row(
                children: [
                  Icon(
                    icon,
                    size: 24.w,
                    color: color ?? AppColors.primary,
                  ),
                  Gap(
                    12.w,
                  ),
                  Text(
                    text,
                    style: regularFont400(
                        color: color ?? AppColors.primary, fontSize: 14.w),
                  )
                ],
              )
            : Text(
                text,
                style: regularFont400(
                    color: color ?? AppColors.primary, fontSize: 14.w),
              ),
      ),
    );
  }
}
