import '../../../../../core/app_colors/app_colors.dart';
import '../../../../../core/export/app_export.dart';
import '../../../../../core/functions/navigate.dart';
import '../../view-model/settings_cubit.dart';
import '../../view-model/settings_state.dart';
import 'option.dart';

class LanguageSelectionDialog extends StatelessWidget {
  const LanguageSelectionDialog({super.key, this.applyButtonFunction});
  final Function? applyButtonFunction;
  @override
  Widget build(BuildContext context) {
    SettingsCubit cubit = SettingsCubit.get(context);
    var tr = S.of(context); // Assuming S.of(context) is the localization method

    return BlocBuilder<SettingsCubit, SettingsState>(builder: (context, state) {
      return AlertDialog(
        backgroundColor: AppColors.background,
        elevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.w))),
        contentPadding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 12.w),
        actionsPadding: EdgeInsetsDirectional.only(
            start: 12.w, end: 24.w, top: 12, bottom: 12.h),
        content: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Option(
                  text: tr.arabic,
                  isSelected: cubit.language == "ar" ? true : false,
                  onTap: () {
                    cubit.changeLanguage(language: "ar");
                  }),
              SizedBox(
                height: 12.h,
              ),
              Option(
                  text: tr.english,
                  isSelected: cubit.language == "en" ? true : false,
                  onTap: () {
                    cubit.changeLanguage(language: "en");
                  }),
            ],
          ),
        ),
        actions: [
          InkWell(
            highlightColor: AppColors.background,
            splashColor: AppColors.background,
            onTap: () {
              navigateBack(context: context);
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 6.h),
              child: Text(
                tr.cancel,
                style:
                mediumFont500(color: AppColors.secondary, fontSize: 14.w),
              ),
            ),
          ),
          SizedBox(
            width: 12.w,
          ),
          InkWell(
            highlightColor: AppColors.background,
            splashColor: AppColors.background,
            onTap: () async {
              if (applyButtonFunction != null) {
                await applyButtonFunction!();
              }
              navigateBack(context: context);
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 6.h),
              child: Text(
                tr.apply,
                style: mediumFont500(color: AppColors.accent, fontSize: 14.w),
              ),
            ),
          ),
        ],
      );
    });
  }
}
