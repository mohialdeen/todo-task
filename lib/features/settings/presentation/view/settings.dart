import 'package:task_manager/core/app_constance/app_constance.dart';
import 'package:task_manager/core/functions/loading_animation.dart';
import 'package:task_manager/core/languages_manager/language_constants.dart';
import 'package:task_manager/core/widgets/custom_second_app_bar.dart';
import 'package:task_manager/features/auth/presentation/view/log_in_screen.dart';
import 'package:task_manager/features/main_home/presentation/view/main_home.dart';
import 'package:task_manager/features/settings/presentation/view-model/settings_cubit.dart';
import 'package:task_manager/features/settings/presentation/view-model/settings_state.dart';
import 'package:task_manager/features/settings/presentation/view/widgets/select_langauge_dialog.dart';
import 'package:task_manager/features/settings/presentation/view/widgets/select_them_dialog.dart';
import 'package:task_manager/features/settings/presentation/view/widgets/settings_option.dart';
import 'package:task_manager/main.dart';

import '../../../../core/export/app_export.dart';

class Settings extends StatelessWidget {
  const Settings({super.key});

  @override
  Widget build(BuildContext context) {
    SettingsCubit cubit = SettingsCubit.get(context);
    var tr = S.of(context);

    return BlocBuilder<SettingsCubit, SettingsState>(builder: (context, state) {
      return PopScope(
        canPop: false,
        onPopInvoked: (didPop) {
          if (didPop) {
            return;
          }
          Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => MainHome()),
            (route) => false,
          );
        },
        child: Scaffold(
          backgroundColor: AppColors.background,
          appBar: CustomSecondAppBar(
              title: tr.settings,
              onLeadingTap: () {
                Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => MainHome()),
                  (route) => false,
                );
              }),
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SettingsOption(
                  text: tr.theme,
                  icon: AppIcons.theme,
                  onTap: () {
                    cubit.isLight = AppColors.isLightTheme;
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return ThemeSelectionDialog(
                          applyButtonFunction: () async {
                            if (cubit.isLight !=
                                CacheHelper.getData(key: "isLightTheme")) {
                              print(cubit.isLight);
                              print(CacheHelper.getData(key: "isLightTheme"));
                              loadingAnimation(context);
                              await Future.delayed(Duration(seconds: 2), () {
                                CacheHelper.saveData(
                                    key: "isLightTheme", value: cubit.isLight);
                                themeManager.toggleTheme(!cubit.isLight);
                                AppColors.isLightTheme = cubit.isLight;
                                navigateBack(context: context);
                              });
                            }
                          },
                        );
                      },
                    );
                  },
                ),
                SettingsOption(
                  text: tr.language,
                  icon: AppIcons.language,
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return LanguageSelectionDialog(
                          applyButtonFunction: () async {
                            if (cubit.language !=
                                CacheHelper.getData(key: AppKey.languageCode)) {
                              print(cubit.language);
                              print(CacheHelper.getData(
                                  key: AppKey.languageCode));
                              loadingAnimation(context);
                              await Future.delayed(Duration(seconds: 2),
                                  () async {
                                Locale _local = await setLocale(cubit.language);
                                TaskManager.setLocale(context, _local);
                                navigateBack(context: context);
                              });
                            }
                          },
                        );
                      },
                    );
                  },
                ),
                SettingsOption(
                  text: tr.log_out,
                  icon: AppIcons.logout,
                  onTap: () {
                    DBHelper.clearToDoTable();
                    DBHelper.clearNotificationTable();
                    DBHelper.clearTaskTable();
                    CacheHelper.deleteData();
                    navigateAndRemoveUntil(context: context, screen: LogInScreen());
                  },
                )
              ],
            ),
          ),
        ),
      );
    });
  }
}
