
sealed class SettingsState {}

final class SettingsInitialState extends SettingsState {}
final class LightThemeState extends SettingsState {}
final class DarkThemeState extends SettingsState {}
final class ArabicLanguageState extends SettingsState {}
final class EnglishLanguageState extends SettingsState {}