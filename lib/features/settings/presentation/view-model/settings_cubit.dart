
import 'package:task_manager/features/settings/presentation/view-model/settings_state.dart';

import '../../../../core/export/app_export.dart';


class SettingsCubit extends Cubit<SettingsState> {
  SettingsCubit() : super(SettingsInitialState());

  static SettingsCubit get(context) => BlocProvider.of(context);
  bool isLight = AppColors.isLightTheme;
  String language = CacheHelper.getData(key: AppKey.languageCode);

  selectLightTheme() {
    isLight = true;
    emit(LightThemeState());
  }

  selectDarkTheme() {
    isLight = false;
    emit(DarkThemeState());
  }

  changeLanguage({required language}) {
    this.language=language;
    emit(ArabicLanguageState());
  }


}
