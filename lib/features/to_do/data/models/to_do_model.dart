// To parse this JSON data, do
//
//     final toDoModel = toDoModelFromJson(jsonString);

import 'dart:convert';

ToDoModel toDoModelFromJson(String str) => ToDoModel.fromJson(json.decode(str));

String toDoModelToJson(ToDoModel data) => json.encode(data.toJson());

class ToDoModel {
  int? id;
  String? todo;
  bool? completed;
  int? userId;
  int? saved = 1;


  ToDoModel({
    this.id,
    this.todo,
    this.completed,
    this.userId,
    this.saved
  });

  factory ToDoModel.fromJson(Map<String, dynamic> json) {
    bool? completedValue;
    if (json["completed"] is bool) {
      completedValue = json["completed"];
    } else if (json["completed"] is int) {
      completedValue = json["completed"] == 1;
    }

    return ToDoModel(
      id: json["id"],
      todo: json["todo"],
      completed: completedValue,
      userId: json["userId"],
      saved: json["saved"],
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "todo": todo,
    "completed": completed! ? 1 : 0, // store as integer (1 or 0)
    "userId": userId,
  };
}
