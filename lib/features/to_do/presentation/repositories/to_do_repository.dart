import 'package:dartz/dartz.dart';
import 'package:task_manager/core/api_services/api_consumer.dart';
import 'package:task_manager/core/errors/exceptions.dart';
import 'package:task_manager/core/export/app_export.dart';
import 'package:task_manager/features/auth/data/models/user_model.dart';
import 'package:task_manager/features/to_do/data/models/to_do_model.dart';

class ToDoRepository {
  final ApiConsumer api;

  ToDoRepository({required this.api});

  /// Returns either a [UserModel] if sign in is successful or a [String] error message if sign in fails.

  Future<Either<String, List<ToDoModel>>> getTodo({
    required limit,
    required skip,
  }) async {
    try {
      final response = await api.get(EndPoint.getTodo,
          queryParameters: {"limit": limit, "skip": skip});
      List<ToDoModel> taskList = [];
      response["todos"].forEach((element) {
        ToDoModel toDoModel = ToDoModel.fromJson(element);
        taskList.add(toDoModel);
      });
      return Right(taskList);
    } on ServerException catch (e) {
      return Left(e.errModel.error.toString());
    }
  }

  Future<Either<String, ToDoModel>> addTodo({
    required  todo,
    required  completed,
    required userId,
  }) async {
    try {
      final response = await api.post(EndPoint.addTodo, data: {
        "todo": todo,
        "completed": completed,
        "userId": userId,
      });

      ToDoModel toDoModel=ToDoModel.fromJson(response);
      return Right(toDoModel);
    } on ServerException catch (e) {
      return Left(e.errModel.error.toString());
    }
  }

  Future<Either<String, ToDoModel>> deleteTodo({
    required todoId,
  }) async {
    try {
      final response = await api.delete(
        EndPoint.deleteTodo(todoId: todoId),
      );

      ToDoModel toDoModel = ToDoModel();
      return Right(toDoModel);
    } on ServerException catch (e) {
      return Left(e.errModel.error.toString());
    }
  }

  Future<Either<String, ToDoModel>> updateTodo({
    required todoId,
    required  completed,
  }) async {
    try {
      final response = await api.patch(EndPoint.updateTodo(todoId: todoId),
          data: {"completed": completed});

      ToDoModel toDoModel = ToDoModel();
      return Right(toDoModel);
    } on ServerException catch (e) {
      return Left(e.errModel.error.toString());
    }
  }
}
