import 'package:flutter/material.dart';
import 'package:task_manager/core/export/app_export.dart';
import 'package:task_manager/features/to_do/data/models/to_do_model.dart';
import 'package:task_manager/features/to_do/presentation/view/widgets/todo_buttom_sheet.dart';

class CustomToDoItem extends StatelessWidget {
  CustomToDoItem({super.key, required this.toDo});

  final ToDoModel toDo;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.only(top: 10),
      child: Container(
        padding: EdgeInsetsDirectional.all(10.w),
        decoration: ShapeDecoration(
          color: AppColors.base,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.w)),
          shadows: [AppShadows.cardShadow()],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 30.w,
                  height: 30.w,

                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: AppColors.secondary, width: 1.w),
                  ),
                  child: Center(
                    child: Text(
                      toDo.id.toString(),
                      style: regularFont400(
                        color: AppColors.secondary,
                        fontSize: 14.w,
                      ),
                    ),
                  ),
                ),
                Gap(12.w),
                Expanded(
                  child: Text(
                    toDo.todo.toString(),
                    style: regularFont400(
                      color: AppColors.accent,
                      fontSize: 14.w,
                    ),
                    overflow: TextOverflow.visible,
                  ),
                ),
                InkWell(
                  onTap: () {
                    showModalBottomSheet(
                      elevation: 0,
                      backgroundColor: AppColors.background,
                      context: context,
                      builder: (BuildContext context) {
                        return ToDoButtonSheet(
                          toDo: toDo,
                        );
                      },
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(1.0),
                    child: Icon(
                      AppIcons.options,
                      color: AppColors.primary,
                      size: 20.w,
                    ),
                  ),
                ),
              ],
            ),
            Gap(10.h),
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 8),
              child: Row(
                children: [
                  Container(
                    width: 12.w,
                    height: 12.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: toDo.completed! ? AppColors.green : AppColors.red,
                    ),
                    child: toDo.completed!
                        ? Icon(
                            Icons.check,
                            size: 10.w,
                            color: AppColors.white,
                          )
                        : null,
                  ),
                  SizedBox(width: 8.w),
                  Text(
                    toDo.completed!
                        ? S.of(context).completed
                        : S.of(context).not_completed,
                    style: regularFont400(
                      color: toDo.completed! ? AppColors.green : AppColors.red,
                      fontSize: 10.w,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
