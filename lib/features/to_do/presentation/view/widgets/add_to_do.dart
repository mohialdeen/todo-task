import 'package:flutter/material.dart';
import 'package:task_manager/core/export/app_export.dart';
import 'package:task_manager/core/widgets/custom_enter_data.dart';
import 'package:task_manager/features/to_do/presentation/view_model/to_do_cubit.dart';

class AddToDoDialog extends StatelessWidget {
  final TextEditingController todoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var tr = S.of(context);
    final _formKey = GlobalKey<FormState>();

    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0.r),
      ),
      backgroundColor: AppColors.background,
      title: Text(
        S.of(context).new_toDo,
        style: regularFont400(color: AppColors.primary, fontSize: 16.w),
      ),
      content: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              CustomEnterData(
                title: tr.todo,
                controller: todoController,
                hintText: tr.what_you_want_to_do,
                textInputType: TextInputType.text,
                validator: (val) {
                  if (val!.isEmpty) {
                    return S.of(context).please_enter_what_you_want_to_do;
                  }
                  return null;
                },
              ),
            ],
          ),
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () {
            Navigator.of(context).pop();

            // if (formKey.currentState!.validate()) {
            //
            // }
          },
          child: Text(
            S.of(context).cancel,
            style: regularFont400(color: AppColors.black),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor: AppColors.secondary,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0.r),
            ),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              ToDoCubit.get(context).addToDo(
                todo: todoController.text,
                userId: CacheHelper.getData(key: AppKey.userId),
                completed: false,
              );
            }
          },
          child: Text(
            S.of(context).create,
            style: regularFont400(color: AppColors.white),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor: AppColors.accent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0.r),
            ),
          ),
        ),
      ],
    );
  }
}
