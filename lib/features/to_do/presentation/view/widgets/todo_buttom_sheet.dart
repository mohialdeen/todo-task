// ignore_for_file: prefer_const_constructors

import 'package:task_manager/core/functions/custom_snack_bar.dart';
import 'package:task_manager/core/functions/loading_animation.dart';
import 'package:task_manager/core/widgets/custom_list_tile.dart';
import 'package:task_manager/core/widgets/rounded_divider.dart';
import 'package:task_manager/features/to_do/data/models/to_do_model.dart';
import 'package:task_manager/features/to_do/presentation/view_model/to_do_cubit.dart';

import '../../../../../core/export/app_export.dart';

class ToDoButtonSheet extends StatelessWidget {
  ToDoButtonSheet({super.key, required this.toDo});

  ToDoModel toDo;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ToDoCubit, ToDoState>(
      listener: (context, state) {
        if (state is DeleteToDoLoadingState ||
            state is UpdateToDoLoadingState) {
          loadingAnimation(context);
        }
        if (state is DeleteToDoErrorState || state is UpdateToDoErrorState) {
          navigateBack(context: context);
          navigateBack(context: context);
          customSnackBar(
              context: context,
              label: S.of(context).something_went_wrong_try_again,
              color: AppColors.secondaryRed,
              fontColor: AppColors.redFontColor);
        }
        if (state is DeleteToDoSuccessState ||
            state is UpdateToDoSuccessState) {
          navigateBack(context: context);
          navigateBack(context: context);
          customSnackBar(
              context: context,
              label: S.of(context).completed_successfully,
              color: AppColors.secondaryAccent,
              fontColor: AppColors.accentFontColor);
        }
      },
      builder: (context, state) {
        return Container(
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(24.r),
                topRight: Radius.circular(24.r),
              ),
            ),
          ),
          child: Padding(
            padding: EdgeInsetsDirectional.symmetric(vertical: 12.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Center(child: RoundedDivider()),
                Gap(12.h),
                Padding(
                  padding: EdgeInsetsDirectional.only(
                      bottom: 12.h, start: 24.w, end: 24.w),
                  child: Row(
                    children: [
                      Container(
                        width: 30.w,
                        height: 30.w,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: AppColors.secondary, width: 1.w),
                        ),
                        child: Center(
                          child: Text(
                            toDo.id.toString(),
                            style: regularFont400(
                              color: AppColors.secondary,
                              fontSize: 14.w,
                            ),
                          ),
                        ),
                      ),
                      Gap(12.w),
                      Expanded(
                        child: Text(
                          toDo.todo.toString(),
                          style: regularFont400(
                            color: AppColors.accent,
                            fontSize: 14.w,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                ),
                CustomListTile(
                  onTap: () {
                    ToDoCubit.get(context).saveToDo(
                        id: toDo.id!.toInt(),
                        savedValue: toDo.saved == 1 ? 0 : 1);
                    navigateBack(context: context);

                  },
                  title: Row(
                    children: [
                      Text(
                        toDo.saved == 0
                            ? S.of(context).saved
                            : S.of(context).save,
                        style: regularFont400(
                          color: AppColors.secondary,
                          fontSize: 14.w,
                        ),
                      ),
                    ],
                  ),
                  leading: Icon(
                    toDo.saved == 1
                        ? AppIcons.saved_filled
                        : AppIcons.saved_outlined,
                    color: AppColors.secondary,
                  ),
                ),
                CustomListTile(
                  onTap: () {
                    ToDoCubit.get(context).updateToDo(
                        todoId: toDo.id!.toInt(), completed: !toDo.completed!);

                  },
                  title: Row(
                    children: [
                      Text(
                        !toDo.completed!
                            ? S.of(context).completed
                            : S.of(context).not_completed,
                        style: regularFont400(
                          color: !toDo.completed!
                              ? AppColors.green
                              : AppColors.red,
                          fontSize: 12.w,
                        ),
                      ),
                    ],
                  ),
                  leading: Icon(
                    AppIcons.edit_outlined,
                    color: !toDo.completed! ? AppColors.green : AppColors.red,
                  ),
                ),
                CustomListTile(
                  onTap: () {
                    ToDoCubit.get(context).deleteToDo(todoId: toDo.id!.toInt());
                  },
                  title: Text(
                    S.of(context).delete,
                    style: regularFont400(fontSize: 14.w, color: AppColors.red),
                  ),
                  leading: Icon(
                    AppIcons.delete,
                    color: AppColors.red,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
