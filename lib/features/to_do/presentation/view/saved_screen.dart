import 'package:flutter/cupertino.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:task_manager/core/functions/custom_snack_bar.dart';
import 'package:task_manager/core/widgets/custom_app_bar.dart';
import 'package:task_manager/core/widgets/custom_drawer.dart';
import 'package:task_manager/core/widgets/custom_second_app_bar.dart';
import 'package:task_manager/features/to_do/data/models/to_do_model.dart';
import 'package:task_manager/features/to_do/presentation/view/widgets/add_to_do.dart';
import 'package:task_manager/features/to_do/presentation/view/widgets/custom_to_do_item.dart';
import 'package:task_manager/features/to_do/presentation/view_model/to_do_cubit.dart';

import '../../../../core/export/app_export.dart';
import '../../../../core/functions/loading_animation.dart';
import '../../../../core/shimmer/custom_to_doItem_shimmer.dart';

class SavedScreen extends StatelessWidget {
  SavedScreen({super.key});

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var cubit = ToDoCubit.get(context);

    var t = S.of(context);
    return BlocConsumer<ToDoCubit, ToDoState>(
      listener: (context, state) {},
      builder: (context, state) {
        return Scaffold(
          key: _scaffoldKey,

          appBar: CustomSecondAppBar(
            title: S.of(context).saved,
          ),
          body:
               ListView.builder(
                  shrinkWrap: true,
                  itemCount: cubit.toDoList.length,
                  itemBuilder: (context, index) {
                    if (cubit.toDoList[index].saved == 1) {
                      ToDoModel toDo = cubit.toDoList[index];
                      return Padding(
                        padding:
                            EdgeInsetsDirectional.symmetric(horizontal: 12.w),
                        child: AnimationConfiguration.staggeredList(
                          position: index,
                          child: SlideAnimation(
                            duration: Duration(microseconds: 50),
                            child: FadeInAnimation(
                              duration: Duration(milliseconds: 50),
                              child: Column(
                                children: [
                                  CustomToDoItem(
                                    toDo: toDo,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    }
                    return Container();
                  },
                ),
        );
      },
    );
  }
}
