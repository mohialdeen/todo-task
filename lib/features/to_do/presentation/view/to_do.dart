import 'package:flutter/cupertino.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:task_manager/core/functions/custom_snack_bar.dart';
import 'package:task_manager/core/widgets/custom_app_bar.dart';
import 'package:task_manager/core/widgets/custom_drawer.dart';
import 'package:task_manager/features/to_do/data/models/to_do_model.dart';
import 'package:task_manager/features/to_do/presentation/view/widgets/add_to_do.dart';
import 'package:task_manager/features/to_do/presentation/view/widgets/custom_to_do_item.dart';
import 'package:task_manager/features/to_do/presentation/view_model/to_do_cubit.dart';

import '../../../../core/export/app_export.dart';
import '../../../../core/functions/loading_animation.dart';
import '../../../../core/shimmer/custom_to_doItem_shimmer.dart';

class ToDo extends StatelessWidget {
  ToDo({super.key});

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var cubit = ToDoCubit.get(context);
    cubit.toDoController = RefreshController(initialRefresh: false);
    var t = S.of(context);
    return BlocConsumer<ToDoCubit, ToDoState>(
      listener: (context, state) {
        if (state is GetToDoErrorState) {
          customSnackBar(
              context: context,
              label: state.error,
              color: AppColors.secondaryRed,
              fontColor: AppColors.redFontColor);
        }
        if (
            state is AddToDoLoadingState) {
          loadingAnimation(context);
        }
        if (state is AddToDoErrorState ) {
          navigateBack(context: context);
          navigateBack(context: context);
          customSnackBar(
              context: context,
              label: S.of(context).something_went_wrong_try_again,
              color: AppColors.secondaryRed,
              fontColor: AppColors.redFontColor);
        }
        if (
            state is AddToDoSuccessState) {
          navigateBack(context: context);
          navigateBack(context: context);
          customSnackBar(
              context: context,
              label: S.of(context).completed_successfully,
              color: AppColors.secondaryAccent,
              fontColor: AppColors.accentFontColor);
        }
      },
      builder: (context, state) {
        return Scaffold(
          key: _scaffoldKey,
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AddToDoDialog();
                },
              );
            },
            backgroundColor: AppColors.accent,
            shape: CircleBorder(),
            child: Icon(
              size: 24.w,
              Icons.add,
              color: AppColors.white,
            ),
          ),
          appBar: CustomAppBar(
            title: S.of(context).todo,
            onMenuPressed: () {
              _scaffoldKey.currentState?.openDrawer();
            },
          ),
          drawer: CustomDrawer(),
          body:(state is GetToDoLoadingState&&cubit.toDoList.isEmpty)?CustomToDoItemShimmer(): SmartRefresher(
            controller: cubit.toDoController,
            enablePullDown: true,
            enablePullUp: true,
            onRefresh: () async {
              await cubit.getToDo(isRefresh: true);
            },
            onLoading: () async {
              await cubit.getToDo();
            },
            footer: CustomFooter(
              builder: (BuildContext context, LoadStatus? mode) {
                Widget body;
                if (mode == LoadStatus.idle) {
                  body = Text(t.pull_up,
                      style: regularFont400(color: AppColors.primary));
                } else if (mode == LoadStatus.loading) {
                  body = CupertinoActivityIndicator(
                    color: AppColors.primary,
                    animating: true,
                  );
                } else if (mode == LoadStatus.failed) {
                  body = Text(t.load_failed,
                      style: regularFont400(color: AppColors.primary));
                } else if (mode == LoadStatus.canLoading) {
                  body = Text(t.loa_more,
                      style: regularFont400(color: AppColors.primary));
                } else {
                  body = Text(t.no_more_tasks,
                      style: regularFont400(color: AppColors.primary));
                }
                return SizedBox(
                  height: 55.0,
                  child: Center(child: body),
                );
              },
            ),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: cubit.toDoList.length,
              itemBuilder: (context, index) {
                ToDoModel toDo = cubit.toDoList[index];
                return Padding(
                  padding: EdgeInsetsDirectional.symmetric(horizontal: 12.w),
                  child: AnimationConfiguration.staggeredList(
                    position: index,
                    child: SlideAnimation(
                      duration: Duration(microseconds: 50),
                      child: FadeInAnimation(
                        duration: Duration(milliseconds: 50),
                        child: Column(
                          children: [
                            CustomToDoItem(
                              toDo: toDo,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }
}
