part of 'to_do_cubit.dart';

@immutable
sealed class ToDoState {}

final class ToDoInitial extends ToDoState {}

final class GetToDoLoadingState extends ToDoState {}

final class GetToDoSuccessState extends ToDoState {}

final class GetToDoErrorState extends ToDoState {
  final String error;

  GetToDoErrorState(this.error);
}

final class AddToDoLoadingState extends ToDoState {}

final class AddToDoSuccessState extends ToDoState {}

final class AddToDoErrorState extends ToDoState {
  final String error;

  AddToDoErrorState(this.error);
}

final class DeleteToDoLoadingState extends ToDoState {}

final class DeleteToDoSuccessState extends ToDoState {}

final class DeleteToDoErrorState extends ToDoState {
  final String error;

  DeleteToDoErrorState(this.error);
}

final class UpdateToDoLoadingState extends ToDoState {}

final class UpdateToDoSuccessState extends ToDoState {}

final class UpdateToDoErrorState extends ToDoState {
  final String error;

  UpdateToDoErrorState(this.error);
}
final class GetLocalToDoLoadingState extends ToDoState {}

final class GetLocalToDoSuccessState extends ToDoState {}

final class GetLocalToDoErrorState extends ToDoState {
  final String error;

  GetLocalToDoErrorState(this.error);
}
final class ChangeSaveToDoState extends ToDoState {}
