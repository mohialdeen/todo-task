import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:task_manager/core/export/app_export.dart';
import 'package:task_manager/features/to_do/presentation/repositories/to_do_repository.dart';

import '../../data/models/to_do_model.dart';

part 'to_do_state.dart';

class ToDoCubit extends Cubit<ToDoState> {
  ToDoCubit(this.toDoRepository) : super(ToDoInitial());

  static ToDoCubit get(context) => BlocProvider.of(context);
  final ToDoRepository toDoRepository;

   RefreshController toDoController=RefreshController();

  bool toDoIsLastPage = false;
  int toDoCurrentPage = 0;
  List<ToDoModel> toDoList = [];

  getToDo({bool isRefresh = false}) async {
    if (isRefresh) {
      toDoController.resetNoData();
      toDoCurrentPage = 0;
      toDoIsLastPage = false;
    }
    if (toDoIsLastPage) {
      toDoController.loadNoData();
    }
    emit(GetToDoLoadingState());
    final result =
        await toDoRepository.getTodo(limit: 10, skip: 10 * toDoCurrentPage);

    result.fold((error) {
      isRefresh ? toDoController.refreshFailed() : toDoController.loadFailed();

      return emit(GetToDoErrorState(error));
    }, (todos) async {
      if (todos.length < 10 || todos.isEmpty) {
        toDoIsLastPage = true;
      } else {
        if (isRefresh) {
          await DBHelper.clearToDoTable();
        }
        await DBHelper.insertMultipleTodos(
            todos.map((todo) => todo.toJson()).toList());
        getToDoFromTable();
        toDoCurrentPage++;
      }
      isRefresh
          ? toDoController.refreshCompleted()
          : toDoController.loadComplete();
      emit(GetToDoSuccessState());
    });
  }

  Future<void> addToDo(
      {required todo, required userId, required  completed}) async {
    emit(AddToDoLoadingState());
    final result = await toDoRepository.addTodo(
        todo: todo, completed: completed, userId: userId);
    result.fold((error) {
      return emit(AddToDoErrorState(error));
    }, (todo) async {
      Map<String, dynamic> todoMap = todo.toJson();

      await DBHelper.insertTodo(todoMap);
      toDoList = (await DBHelper.getAllTodos())
          .map((e) => ToDoModel.fromJson(e))
          .toList();
      return emit(AddToDoSuccessState());
    });
  }

  Future<void> deleteToDo({required  todoId}) async {
    emit(DeleteToDoLoadingState());
    final result = await toDoRepository.deleteTodo(todoId: todoId);
    result.fold((error) {
      return emit(DeleteToDoErrorState(error));
    }, (todo) async {
      await DBHelper.deleteTodo(todoId);
      toDoList = (await DBHelper.getAllTodos())
          .map((e) => ToDoModel.fromJson(e))
          .toList();

      return emit(DeleteToDoSuccessState());
    });
  }

  Future<void> updateToDo(
      {required  todoId, required bool completed}) async {
    emit(UpdateToDoLoadingState());
    final result =
        await toDoRepository.updateTodo(todoId: todoId, completed: completed);
    result.fold((error) {
      return emit(UpdateToDoErrorState(error));
    }, (todo) async {
      await DBHelper.updateTodoCompleted(todoId, completed ? 1 : 0);
      toDoList = (await DBHelper.getAllTodos())
          .map((e) => ToDoModel.fromJson(e))
          .toList();
      emit(UpdateToDoSuccessState());
    });
  }

  getToDoFromTable() async {
    emit(GetLocalToDoLoadingState());
    try {
      final List<Map<String, dynamic>> todosMapList =
          await DBHelper.getAllTodos();
      toDoList =
          todosMapList.map((todoMap) => ToDoModel.fromJson(todoMap)).toList();
      emit(GetLocalToDoSuccessState());
    } catch (error) {
      emit(GetLocalToDoErrorState(error.toString()));
    }
  }

  saveToDo({required id, required int savedValue}) async {
    DBHelper.updateSaved(id, savedValue);
    getToDoFromTable();
    emit(ChangeSaveToDoState());
  }
}
