// ignore_for_file: prefer_const_constructors

import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:task_manager/features/schedule/presentation/view_model/schedule_cubit.dart';

import '../../../../../core/export/app_export.dart';

class CustomCalendar extends StatelessWidget {
  const CustomCalendar({super.key});

  @override
  Widget build(BuildContext context) {
    var cubit = ScheduleCubit.get(context);
    return BlocBuilder<ScheduleCubit, ScheduleState>(
      builder: (context, state) {
        return TableCalendar(
          enabledDayPredicate: (day) {
            return day.month == cubit.focusedDay.month;
          },
          onPageChanged: (day) {
            cubit.onPageChanged(day);
          },
          currentDay: cubit.currentDay,
          calendarFormat: cubit.calendarFormat,
          firstDay: DateTime.utc(2010, 10, 20),
          lastDay: DateTime.utc(2040, 10, 20),
          focusedDay: cubit.focusedDay,
          selectedDayPredicate: (day) => isSameDay(cubit.selectedDay, day),
          onDaySelected: (selectedDay, focusedDay) {
            if (!isSameDay(cubit.selectedDay, selectedDay)) {
              cubit.changeDay(selectedDay, focusedDay);
            }
            print(cubit.focusedDay);
            print(cubit.selectedDay);
          },
          daysOfWeekHeight: 44.h,
          daysOfWeekStyle: DaysOfWeekStyle(
              dowTextFormatter: (date, locale) =>
                  DateFormat.E(CacheHelper.getData(key: AppKey.languageCode))
                      .format(date)
                      .toUpperCase(),
              weekdayStyle: regularFont400(
                  fontSize:
                      CacheHelper.getData(key: AppKey.languageCode) == "en"
                          ? 16.w
                          : 12.w,
                  color: AppColors.secondary)),
          locale: 'en',
          headerVisible: false,
          weekendDays: [],
          calendarStyle: CalendarStyle(
            outsideDaysVisible: true,
            selectedDecoration: BoxDecoration(
              color: AppColors.accent,
              shape: BoxShape.circle,
            ),
            todayDecoration: BoxDecoration(
              color: AppColors.accent.withOpacity(0.25),
              shape: BoxShape.circle,
            ),
            selectedTextStyle:
                mediumFont500(color: AppColors.white, fontSize: 16.w),
            todayTextStyle:
                mediumFont500(color: AppColors.accent, fontSize: 16.w),
            weekendTextStyle:
                mediumFont500(color: AppColors.primary, fontSize: 16.w),
            disabledTextStyle:
                mediumFont500(color: AppColors.secondary, fontSize: 14.w),
            defaultTextStyle:
                mediumFont500(color: AppColors.primary, fontSize: 16.w),
            holidayTextStyle:
                mediumFont500(color: AppColors.primary, fontSize: 16.w),
          ),
        );
      },
    );
  }
}
