
import 'package:task_manager/features/schedule/data/models/task_model.dart';
import 'package:task_manager/features/schedule/presentation/view/widgets/custom_buttom_sheet.dart';
import 'package:task_manager/features/schedule/presentation/view_model/schedule_cubit.dart';

import '../../../../../core/export/app_export.dart';

class CustomTaskItem extends StatelessWidget {
  CustomTaskItem({super.key, required this.task});

  TaskModel task;

  @override
  Widget build(BuildContext context) {
    var cubit = ScheduleCubit.get(context);
    return Container(
      padding: EdgeInsetsDirectional.all(8.w),
      decoration: ShapeDecoration(
        color: AppColors.base,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.w)),
        shadows: [
          AppShadows.cardShadow()
        ],
      ),
      child: BlocBuilder<ScheduleCubit, ScheduleState>(
        builder: (context, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    task.taskName.toString(),
                    style: mediumFont500(
                        color:  AppColors.accent,

                        fontSize: 16.w),
                  ),
                  Spacer(),
                  InkWell(
                      onTap: () {
                        showModalBottomSheet(
                          elevation: 0,backgroundColor: AppColors.background,
                          context: context,
                          builder: (BuildContext context) {
                            return CustomButtonSheet(

                              title: task.taskName.toString(),
                              deleteTaskFun: () {
                                cubit.deleteTask(task);
                                navigateBack(context: context);
                              },
                            );
                          },
                        );
                      },
                      child: Padding(
                        padding: EdgeInsets.all(1.0),
                        child: Icon(
                          AppIcons.options,
                          color: AppColors.primary,
                          size: 20.w,
                        ),
                      )),
                ],
              ),
              Gap(4.h),
              cubit.showAll
                  ? Text(
                      task.date.toString(),
                      style: regularFont400(
                          color: AppColors.primary, fontSize: 14.w),
                    )
                  : Container(),
              Gap(8.h),
              Text(
                "${task.startTime.toString()} - ${task.endTime.toString()} ",
                style: regularFont400(
                    color: AppColors.secondary, fontSize: 14.w),
              )
            ],
          );
        },
      ),
    );
  }
}
