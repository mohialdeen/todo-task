import 'package:intl/intl.dart';
import 'package:task_manager/features/schedule/presentation/view/widgets/calendar_Format.dart';
import 'package:task_manager/features/schedule/presentation/view_model/schedule_cubit.dart';

import '../../../../../core/export/app_export.dart';
import '../../../../../core/widgets/non_changed_icon_button.dart';

class CustomCalendarHeader extends StatelessWidget {
  const CustomCalendarHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ScheduleCubit, ScheduleState>(
      listener: (context, state) {
        // TODO: implement listener
      },
      builder: (context, state) {
        return Container(
          child: Padding(
            padding: EdgeInsetsDirectional.symmetric(
                horizontal: 12.w, vertical: 12.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    //TODO: open date picker and select year and month
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        DateFormat('yyyy MMM', 'en_US')
                            .format(ScheduleCubit.get(context).focusedDay)
                            .toString(),
                        style: mediumFont500(
                            color: AppColors.primary, fontSize: 20.w),
                      ),
                      Icon(
                        CacheHelper.getData(key: AppKey.languageCode)=="en"?
                        AppIcons.polygon: AppIcons.polygon2,
                        size: 10.w,
                        color: AppColors.primary,
                      )
                    ],
                  ),
                ),
                Row(
                  children: [
                    TextCheckBox(
                        text: S.of(context).show_all,
                        value: ScheduleCubit.get(context).showAll,
                        onChange: (value) {

                          ScheduleCubit.get(context).changeShowAll();
                        }),
                    Gap(12.w),
                    NonChangeIconButton(

                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (context) => CalendarFormat());
                      },
                      icon: ScheduleCubit.get(context).calendarFormatString ==
                              "month"
                          ? AppIcons.monthly_calender
                          : AppIcons.weekly_calender,
                      size: 32.w,
                      color: AppColors.secondary,

                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}


//
// class TextCheckBox extends StatelessWidget {
//   const TextCheckBox({
//     super.key,
//   });
//
//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       children: [
//         Text(
//           S.of(context).show_all,
//           style: mediumFont500(
//               color: AppColors.secondary, fontSize: 12.w),
//         ),
//         CustomCheckBox(
//           value: ScheduleCubit.get(context).showAll,
//           onChange: (value) {
//             //TODO: show all Events methode
//             ScheduleCubit.get(context).changeShowAll();
//           },
//           color: AppColors.secondary,
//         ),
//       ],
//     );
//   }
// }

class TextCheckBox extends StatefulWidget {
  const TextCheckBox({
    super.key,
    required this.text,
    this.style,
    this.uncheckColor,
    this.checkColor,
    required this.value,
    required this.onChange,
  });

  final String text;
  final TextStyle? style;
  final Color? uncheckColor;
  final Color? checkColor;
  final bool value;
  final ValueChanged<bool?> onChange;

  @override
  State<TextCheckBox> createState() => _TextCheckBoxState();
}

class _TextCheckBoxState extends State<TextCheckBox> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        setState(() {
          widget.onChange(!widget.value);
        });
      },
      child: Row(
        children: [
          Text(widget.text,
              style: widget.style ??
                  mediumFont500(
                      color: widget.value
                          ? widget.checkColor ?? AppColors.accent
                          : widget.uncheckColor ?? AppColors.secondary,
                      fontSize: 12.w)),
          Gap(8.w), // Add some spacing between text and checkbox
          Icon(
            widget.value ? AppIcons.checked : AppIcons.check,
            color: widget.value
                ? widget.checkColor ?? AppColors.accent
                : widget.uncheckColor ?? AppColors.secondary,
            size: 14.w,
          ),
        ],
      ),
    );
  }
}
