// ignore_for_file: prefer_const_constructors

import 'package:task_manager/core/widgets/custom_list_tile.dart';
import 'package:task_manager/core/widgets/rounded_divider.dart';
import 'package:task_manager/features/schedule/presentation/view_model/schedule_cubit.dart';

import '../../../../../core/export/app_export.dart';

class CustomButtonSheet extends StatelessWidget {
  CustomButtonSheet(
      {super.key,
      this.title,
      required this.deleteTaskFun,
      this.cancelServiceRequest});

  String? title;
  GestureTapCallback? deleteTaskFun;
  GestureTapCallback? cancelServiceRequest;

  @override
  Widget build(BuildContext context) {
    var cubit = ScheduleCubit.get(context);
    return Container(
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24.r),
            topRight: Radius.circular(24.r),
          ),
        ),
      ),
      child: Padding(
        padding: EdgeInsetsDirectional.symmetric(vertical: 12.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Center(child: RoundedDivider()),
            Gap(12.h),
            title != null
                ? Padding(
                    padding: EdgeInsetsDirectional.only(
                        bottom: 12.h, start: 24.w, end: 24.w),
                    child: Text(
                      title.toString(),
                      style: mediumFont500(
                        fontSize: 16.w,
                        color: AppColors.accent,
                      ),
                    ),
                  )
                : Container(),
            // CustomListTile(
            //   title: Text(
            //     S.of(context).edit_task,
            //     style: regularFont400(fontSize: 14.w, color: AppColors.primary),
            //   ),
            //   leading: Icon(AppIcons.edit_outlined),
            // ),
            CustomListTile(
              onTap: deleteTaskFun,
              title: Text(
                S.of(context).delete_task,
                style: regularFont400(fontSize: 14.w, color: AppColors.red),
              ),
              leading: Icon(
                AppIcons.delete,
                color: AppColors.red,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
