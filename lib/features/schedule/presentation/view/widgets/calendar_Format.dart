// ignore_for_file: prefer_const_constructors


import 'package:task_manager/features/schedule/presentation/view_model/schedule_cubit.dart';

import '../../../../../core/export/app_export.dart';

class CalendarFormat extends StatelessWidget {
  const CalendarFormat({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        contentPadding: EdgeInsets.zero,
        insetPadding: const EdgeInsets.all(12),
        content: Container(
          width: 280.w,
          height: 165.h,
          decoration: ShapeDecoration(
            color: AppColors.background,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.r),
            ),
          ),
          child: Padding(
            padding: EdgeInsetsDirectional.symmetric(
                vertical: 8.h, horizontal: 12.w),
            child: BlocBuilder<ScheduleCubit, ScheduleState>(
              builder: (context, state) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    _buildFormatMonthRow(context),
                    Gap(8.h),
                    _buildFormatWeekRow(context),
                  ],
                );
              },
            ),
          ),
        ));
  }

  Widget _buildFormatMonthRow(context) {
    return InkWell(
      onTap: () {
        ScheduleCubit.get(context).changeCalendarFormat("month");
      },
      child: Container(
        decoration: ShapeDecoration(
          color: ScheduleCubit.get(context).calendarFormatString == "month"
              ? AppColors.gray
              : AppColors.background,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.r)),
        ),
        child: Padding(
          padding:
              EdgeInsetsDirectional.symmetric(vertical: 12.h, horizontal: 12.w),
          child: Row(
            children: [
              Icon(
                AppIcons.monthly_calender,
                size: 35,
                color:
                    ScheduleCubit.get(context).calendarFormatString == "month"
                        ? AppColors.accent
                        : AppColors.primary,
              ),
              Gap(8.w),
              Text(
                S.of(context).month,
                style: mediumFont500(
                  fontSize: 14.w,
                  color:
                      ScheduleCubit.get(context).calendarFormatString == "month"
                          ? AppColors.accent
                          : AppColors.primary,
                ),
              ),
              Spacer(),
              ScheduleCubit.get(context).calendarFormatString == "month"
                  ? Icon(
                      AppIcons.radio_selected,
                      color: AppColors.accent,
                    )
                  : Icon(
                      AppIcons.radio_unselceted,
                      color: AppColors.primary,
                    ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFormatWeekRow(context) {
    return InkWell(
      onTap: () {
        ScheduleCubit.get(context).changeCalendarFormat("week");
      },
      child: Container(
        padding:
            EdgeInsetsDirectional.symmetric(vertical: 12.h, horizontal: 12.w),
        decoration: ShapeDecoration(
          color: ScheduleCubit.get(context).calendarFormatString == "month"
              ? AppColors.background
              : AppColors.gray,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.r)),
        ),
        child: Row(
          children: [
            Icon(
              AppIcons.weekly_calender,
              size: 35,
              color: ScheduleCubit.get(context).calendarFormatString == "month"
                  ? AppColors.primary
                  : AppColors.accent,
            ),
            Gap(8.w),
            Text(
              S.of(context).week,
              style: mediumFont500(
                  fontSize: 14.w,
                  color:
                      ScheduleCubit.get(context).calendarFormatString == "month"
                          ? AppColors.primary
                          : AppColors.accent),
            ),
            Spacer(),
            ScheduleCubit.get(context).calendarFormatString == "month"
                ? Icon(
                    AppIcons.radio_unselceted,
                    color: AppColors.primary,
                  )
                : Icon(
                    AppIcons.radio_selected,
                    color: AppColors.accent,
                  ),
          ],
        ),
      ),
    );
  }
}
