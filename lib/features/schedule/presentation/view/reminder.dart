
import 'package:task_manager/core/widgets/custom_cupertino_switch.dart';
import 'package:task_manager/core/widgets/custom_second_app_bar.dart';
import 'package:task_manager/features/schedule/data/models/reminder_model.dart';
import 'package:task_manager/features/schedule/presentation/view_model/schedule_cubit.dart';

import '../../../../core/export/app_export.dart';

class Reminder extends StatelessWidget {
  Reminder({super.key});


  @override
  Widget build(BuildContext context) {
    var tr = S.of(context);
    String language = CacheHelper.getData(key: AppKey.languageCode);
    var cubit = ScheduleCubit.get(context);
    return Scaffold(
      appBar: CustomSecondAppBar(
        title: tr.reminder,
      ),
      body: Padding(
        padding: EdgeInsets.all(12),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  tr.reminder,
                  style:
                      regularFont400(color: AppColors.primary, fontSize: 14.w),
                ),
                Spacer(),
                BlocBuilder<ScheduleCubit, ScheduleState>(
                  builder: (context, state) {
                    return CustomCupertinoSwitch(
                      active: true,
                      value: ScheduleCubit.get(context).reminder,
                      onChange: (newValue) {
                        cubit.changeSwitcher(newValue);
                      },
                    );
                  },
                ),
              ],
            ),
            Gap(12.h),
            BlocBuilder<ScheduleCubit, ScheduleState>(
              builder: (context, state) {
                return ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) =>
                      RadioListTile(
                    controlAffinity: ListTileControlAffinity.trailing,
                    title: Text(
                      language == "en"
                          ? ReminderModel.ReminderList()[index].titleEn
                          : ReminderModel.ReminderList()[index].titleAr,
                      style: regularFont400(
                          color: AppColors.secondary, fontSize: 14.w),
                    ),
                    value: index,
                    groupValue: cubit.reminderOption,
                    materialTapTargetSize: MaterialTapTargetSize.padded,

                    onChanged: (value) {
                      cubit.changeReminderOption(value) ;
                    },
                  ),
                  itemCount: ReminderModel.ReminderList().length,
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
