import 'package:intl/intl.dart';
import 'package:task_manager/core/widgets/custom_cupertino_switch.dart';
import 'package:task_manager/core/widgets/custom_enter_data.dart';
import 'package:task_manager/core/widgets/custom_second_app_bar.dart';
import 'package:task_manager/features/schedule/data/models/reminder_model.dart';
import 'package:task_manager/features/schedule/presentation/view/reminder.dart';
import 'package:task_manager/features/schedule/presentation/view_model/schedule_cubit.dart';

import '../../../../core/export/app_export.dart';
import '../../../../core/widgets/custom_elevated_button.dart';
import '../../data/models/task_model.dart';

class AddTask extends StatelessWidget {
  AddTask({super.key});

  TextEditingController taskNameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController fromController = TextEditingController();
  TextEditingController toController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var tr = S.of(context);
    var cubit = ScheduleCubit.get(context);
    var lang = CacheHelper.getData(key: "LANGUAGE_CODE");

    final formKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: CustomSecondAppBar(
        title: tr.new_task,
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Form(
                key: formKey,
                child: Padding(
                  padding: EdgeInsets.all(12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomEnterData(
                        title: tr.task_name,
                        controller: taskNameController,
                        hintText: tr.enter_the_name_of_the_task,
                        textInputType: TextInputType.text,
                        validator: (val) {
                          if (val!.isEmpty) {
                            return 'This field cannot be empty';
                          }
                        },
                      ),
                      Gap(12.h),
                      CustomEnterData(
                        title: tr.description,
                        controller: descriptionController,
                        hintText: tr.enter_the_description_of_the_task,
                      ),
                      Gap(24.h),
                      CustomEnterData(
                        validator: (val) {
                          if (val!.isEmpty) {
                            return 'This field cannot be empty';
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          cubit.getDateFromUser(context, dateController);
                        },
                        prefix: Icon(
                          AppIcons.calender,
                          color: AppColors.secondary,
                        ),
                        title: tr.date,
                        controller: dateController,
                        hintText: DateFormat('E, dd MMMM yyyy')
                            .format(cubit.newTaskDate),
                      ),
                      Gap(12.h),
                      CustomEnterData(
                        validator: (val) {
                          if (val!.isEmpty) {
                            return 'This field cannot be empty';
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          cubit.getTimeFromUser(
                              context: context, controller: fromController);
                        },
                        prefix: Icon(
                          AppIcons.clock,
                          color: AppColors.secondary,
                        ),
                        title: tr.from,
                        controller: fromController,
                        hintText: tr.start_time,
                      ),
                      Gap(12.h),
                      CustomEnterData(
                        validator: (val) {
                          if (val!.isEmpty) {
                            return 'This field cannot be empty';
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          cubit.getTimeFromUser(
                              context: context, controller: toController);
                        },
                        prefix: Icon(
                          AppIcons.clock,
                          color: AppColors.secondary,
                        ),
                        title: tr.to,
                        controller: toController,
                        hintText: tr.end_time,
                      ),
                      Gap(12.h),
                      Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          border: Border(
                            top: BorderSide(width: 1.w, color: AppColors.gray),
                            bottom:
                                BorderSide(width: 1.w, color: AppColors.gray),
                          ),
                        ),
                        child: InkWell(
                          onTap: () {
                            navigateTo(context: context, screen: Reminder());
                          },
                          splashColor: AppColors.gray,
                          highlightColor: AppColors.gray,
                          child: Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                vertical: 12.h, horizontal: 4.w),
                            child: Row(
                              children: [
                                Text(
                                  tr.reminder,
                                  style: regularFont400(
                                      color: AppColors.primary, fontSize: 14.w),
                                ),
                                Spacer(),
                                BlocBuilder<ScheduleCubit, ScheduleState>(
                                  builder: (context, state) {
                                    return Text(
                                      cubit.reminderOption != null
                                          ? (lang == "en"
                                              ? ReminderModel.ReminderList()[
                                                      cubit.reminderOption
                                                          as int]
                                                  .titleEn
                                              : ReminderModel.ReminderList()[
                                                      int.parse(cubit
                                                          .reminderOption
                                                          .toString())]
                                                  .titleAr)
                                          : "",
                                      style: regularFont400(
                                          color: AppColors.secondary,
                                          fontSize: 12.w),
                                    );
                                  },
                                ),
                                BlocBuilder<ScheduleCubit, ScheduleState>(
                                  builder: (context, state) {
                                    return CustomCupertinoSwitch(
                                      active: cubit.reminder ? true : false,
                                      value:
                                          ScheduleCubit.get(context).reminder,
                                      onChange: (newValue) {
                                        cubit.changeSwitcher(newValue);
                                      },
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 24.w, vertical: 12.h),
            child: CustomElevatedButton(
              onPressed: () async {
                if (formKey.currentState!.validate()) {
                  var newTaskId = await _addTaskToDB(context);
                  _scheduleTaskTask(context, newTaskId);
                  navigateBack(context: context);
                  cubit.getTasksByDate();
                }
              },
              height: 46.h,
              text: S.of(context).create_task,
              buttonTextStyle:
                  mediumFont500(fontSize: 16.w, color: AppColors.white),
              buttonStyle: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0.r),
                ),
                backgroundColor: AppColors.accent,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<int> _addTaskToDB(context) async {
    var cubit = ScheduleCubit.get(context);
    int val = await cubit.addTaskToDB(
        task: TaskModel(
            taskName: taskNameController.text,
            description: descriptionController.text,
            date: dateController.text,
            startTime: fromController.text,
            endTime: toController.text,
            remind: cubit.reminder ? cubit.reminderOption : null,
            customTask: 0,
           ));
    print("new value is : $val");
    return val;
  }

  _scheduleTaskTask(context, int taskId) async {
    var cubit = ScheduleCubit.get(context);
    await cubit.scheduleTask(
        task: TaskModel(
            id: taskId,
            taskName: taskNameController.text,
            description: descriptionController.text,
            date: dateController.text,
            startTime: fromController.text,
            endTime: toController.text,
            remind: cubit.reminder ? cubit.reminderOption : null,
            customTask: 0,
            ));
  }
}
