// ignore_for_file: prefer_const_constructors
//TODO: Abd
import 'package:flutter/cupertino.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:task_manager/core/export/app_export.dart';
import 'package:task_manager/core/widgets/custom_app_bar.dart';
import 'package:task_manager/core/widgets/custom_drawer.dart';
import 'package:task_manager/core/widgets/custom_filter_Button.dart';
import 'package:task_manager/core/widgets/custom_icon_button.dart';
import 'package:task_manager/core/widgets/select_dialog.dart';
import 'package:task_manager/features/schedule/data/models/task_model.dart';
import 'package:task_manager/features/schedule/presentation/view/add_task.dart';
import 'package:task_manager/features/schedule/presentation/view/widgets/custom_calendar.dart';
import 'package:task_manager/features/schedule/presentation/view/widgets/custom_calendar_header.dart';
import 'package:task_manager/features/schedule/presentation/view/widgets/custom_event_item.dart';
import 'package:task_manager/features/schedule/presentation/view_model/schedule_cubit.dart';

import '../../../../core/widgets/rounded_divider.dart';

class Schedule extends StatelessWidget {
  Schedule({Key? key})
      : super(
          key: key,
        );
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController searchController = TextEditingController();
  ScrollController controller = ScrollController();
  String? beforeFormat;

  @override
  Widget build(BuildContext context) {
    var cubit = ScheduleCubit.get(context);
    var t = S.of(context);
    controller.addListener(() {
      if (ScheduleCubit.get(context).calendarFormatString == "month") {
        if (controller.position.pixels > 20.h) {
          ScheduleCubit.get(context).changeCalendarFormat("week");
          beforeFormat = "month";
          print("beforeFormat");
        }
      }
      if (controller.position.pixels < 20.h && beforeFormat == "month") {
        ScheduleCubit.get(context).changeCalendarFormat("month");
        beforeFormat = "";
        print("beforeFormat");
      }
    });

    return BlocConsumer<ScheduleCubit, ScheduleState>(
      listener: (context, state) {
        // TODO: implement listener
      },
      builder: (context, state) {
        return Scaffold(
            key: _scaffoldKey,
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                navigateTo(context: context, screen: AddTask());
              },
              backgroundColor: AppColors.accent,
              shape: CircleBorder(),
              child: Icon(
                size: 24.w,
                Icons.add,
                color: AppColors.white,
              ),
            ),
            appBar: CustomAppBar(
              title: S.of(context).schedule,
              onMenuPressed: () {
                _scaffoldKey.currentState?.openDrawer();
              },
            ),
            drawer: CustomDrawer(),
            body: Column(
              children: [
                CustomCalendarHeader(),
                Padding(
                  padding: EdgeInsetsDirectional.only(start: 12.w, end: 12.w),
                  child: CustomCalendar(),
                ),
                RoundedDivider(),
                Gap(4.h),
                cubit.taskList.isNotEmpty
                    ? Expanded(
                        child: SingleChildScrollView(
                          controller: controller,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: cubit.taskList.length,
                                itemBuilder: (context, index) {
                                  TaskModel task = cubit.taskList[index];

                                  return Padding(
                                    padding: EdgeInsetsDirectional.symmetric(
                                        horizontal: 12.w),
                                    child: AnimationConfiguration.staggeredList(
                                      position: index,
                                      child: SlideAnimation(
                                        duration: Duration(milliseconds: 900),
                                        child: FadeInAnimation(
                                          duration:
                                              Duration(milliseconds: 1500),
                                          child: Column(
                                            children: [
                                              CustomTaskItem(
                                                task: task,
                                              ),
                                              if (index <
                                                  cubit.taskList.length - 1)
                                                Gap(12.h),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      )
                    : Expanded(
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.all(12.w),
                            child: Text(
                              textAlign: TextAlign.center,
                              S.of(context).no_tasks_to_show,
                              style: regularFont400(
                                  color: AppColors.secondary, fontSize: 14.w),
                            ),
                          ),
                        ),
                      )
              ],
            ));
      },
    );
  }
}
