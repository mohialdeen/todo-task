import 'package:date_format/date_format.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:task_manager/core/export/app_export.dart';
import 'package:task_manager/features/schedule/data/models/local_notification.dart';
import 'package:task_manager/features/schedule/data/models/reminder_model.dart';
import 'package:task_manager/features/schedule/data/models/task_model.dart';
import 'package:task_manager/features/schedule/presentation/repositories/schedule_repository.dart';

part 'schedule_state.dart';

class ScheduleCubit extends Cubit<ScheduleState> {
  ScheduleCubit(this.scheduleRepository) : super(ScheduleInitial());
  final ScheduleRepository scheduleRepository;

  static ScheduleCubit get(context) => BlocProvider.of(context);
  bool showAll = false;
  bool reminder = false;
  int? reminderOption;
  CalendarFormat calendarFormat = CalendarFormat.month;
  String calendarFormatString = "month";
  DateTime focusedDay = DateTime.now();
  DateTime currentDay = DateTime.now();
  DateTime? selectedDay = DateTime.now();
  DateTime newTaskDate = DateTime.now();
  var taskList = <TaskModel>[];
  String filterValue = 'All';

  changeShowAll() {
    showAll = !showAll;
    showAll == true ? getAllTasks() : getTasksByDate();
    emit(ChangeShowAll());
  }



  changeCalendarFormat(String format) {
    if (format == "month") {
      calendarFormat = CalendarFormat.month;
      calendarFormatString = "month";
    } else {
      calendarFormat = CalendarFormat.week;
      calendarFormatString = "week";
    }
    print(calendarFormatString);
    emit(ChangeCalendarFormat());
  }

  changeDay(selected, focused) {
    selectedDay = selected;
    focusedDay = focused;
    showAll == false ? getTasksByDate() : null;
    emit(ChangeDay());
  }

  onPageChanged(DateTime focused) {
    focusedDay = focused;
    emit(OnPageChanged());
  }

  changeSwitcher(bool newValue) {
    newValue == false ? reminderOption = null : null;
    reminder = newValue;
    emit(ChangSwitcher());
  }

  changeReminderOption(newValue) {
    reminder == false ? reminder = !reminder : null;
    reminderOption = newValue;
    emit(ChangReminderOption());
  }

  getDateFromUser(
      BuildContext context, TextEditingController controller) async {
    DateTime? _pickerDate = await showDatePicker(
        locale: Locale("en"),
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime(2121),
        switchToCalendarEntryModeIcon: Icon(
          AppIcons.monthly_calender,
          color: AppColors.secondary,
          size: 28.w,
        ),
        switchToInputEntryModeIcon: Icon(
          AppIcons.edit_outlined,
          color: AppColors.secondary,
          size: 24.w,
        ),
        builder: (context, child) {
          return Theme(
            data: Theme.of(context).copyWith(
              colorScheme: AppColors.isLightTheme
                  ? ColorScheme.light(
                      primary: WBHColors.accent,
                      secondary: WBHColors.secondaryLight,
                      surface: WBHColors.backgroundLight,
                      onSurface: WBHColors.primaryLight,
                      outline: AppColors.secondary)
                  : ColorScheme.dark(
                      primary: WBHColors.accent,
                      secondary: WBHColors.secondaryDark,
                      surface: WBHColors.backgroundDark,
                      onSurface: WBHColors.secondaryDark,
                      outline: AppColors.secondary),
              buttonTheme: ButtonThemeData(
                textTheme: ButtonTextTheme.accent,
              ),
              textTheme: TextTheme(
                  titleSmall:
                      mediumFont500(fontSize: 14.w, color: AppColors.secondary),
                  bodyLarge:
                      regularFont400(fontSize: 14.w, color: AppColors.primary),
                  headlineLarge:
                      mediumFont500(fontSize: 20.w, color: AppColors.secondary),
                  labelLarge: mediumFont500(
                      fontSize: 16.w, color: AppColors.secondary)),
            ),
            child: child!,
          );
        });
    if (_pickerDate != null) {
      newTaskDate = _pickerDate;
      controller.text = DateFormat('E, dd MMMM yyyy').format(_pickerDate);
    } else {
      print("it's null or something is wrong");
    }
    emit(GetDateFromUser());
  }

  getTimeFromUser(
      {required BuildContext context,
      required TextEditingController controller}) async {
    var pickedTime = await _showTimePicker(context);
    if (pickedTime != null) {
      String _hour = pickedTime.hour.toString();
      String _minute = pickedTime.minute.toString();
      String _time = _hour + ' : ' + _minute;
      _time = formatDate(
          locale: EnglishDateLocale(),
          DateTime(2023, 08, 1, pickedTime.hour, pickedTime.minute),
          [hh, ':', nn, " ", am]).toString();
      controller.text = _time;
    }
    emit(GetTimeFromUser());
  }

  _showTimePicker(BuildContext context) async {
    return showTimePicker(
        initialEntryMode: TimePickerEntryMode.dial,
        context: context,
        initialTime: TimeOfDay.now(),
        builder: (context, child) {
          return Theme(
            data: Theme.of(context).copyWith(
              colorScheme: AppColors.isLightTheme
                  ? ColorScheme.light(
                      primary: AppColors.accent,
                      secondary: AppColors.accent.withOpacity(0.2),
                      onSecondary: AppColors.accent,
                      onSurface: AppColors.primary,
                      outline: Colors.transparent,
                      surface: AppColors.background)
                  : ColorScheme.dark(
                      primary: AppColors.accent,
                      secondary: AppColors.accent,
                      onSecondary: AppColors.secondary,
                      surface: AppColors.background),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
              textTheme: TextTheme(
                titleSmall:
                    mediumFont500(fontSize: 14.w, color: AppColors.secondary),
                bodyLarge:
                    regularFont400(fontSize: 14.w, color: AppColors.primary),
                headlineLarge:
                    mediumFont500(fontSize: 20.w, color: AppColors.secondary),
                labelLarge:
                    mediumFont500(fontSize: 16.w, color: AppColors.secondary),
              ),
            ),
            child: child!,
          );
        });
  }

  Future<int> addTaskToDB({required TaskModel task}) async {
    return await DBHelper.insertTask(task);
  }

  getTasksByDate() async {
    List<Map<String, dynamic>> tasks = await DBHelper.queryTasks(selectedDay!);
    print(tasks.length);
    taskList.clear();

    tasks.map((data) => TaskModel.fromJson(data)).forEach((task) {
      taskList.add(task);
    });

    print(taskList.length);
    print(selectedDay);
    emit(GetTaskByDate());
  }

  getAllTasks() async {
    List<Map<String, dynamic>> tasks = await DBHelper.queryAllTasks();
    print(tasks.length);
    taskList.clear();

    tasks.map((data) => TaskModel.fromJson(data)).forEach((task) {
      taskList.add(task);
    });

    print(taskList.length);
    print(selectedDay);
    emit(GetAllTask());
  }

  scheduleTask({required TaskModel task}) async {
    LocalNotification notificationModel;
    print(task.startTime);
    print("+++++++++++++++++++++");
    // Converting the start and end time and date of the task from String to DateTime

    DateTime startTime = DateFormat.jm().parse(task.startTime.toString());
    DateTime endTime = DateFormat.jm().parse(task.endTime.toString());
    DateTime dateTime =
        DateFormat('E, dd MMMM yyyy').parse(task.date.toString());
    print(startTime);
    print(endTime);
    print(dateTime);
    DateTime tempStart = DateTime(dateTime.year, dateTime.month, dateTime.day,
        startTime.hour, startTime.minute);
    DateTime tempEnd = DateTime(dateTime.year, dateTime.month, dateTime.day,
        endTime.hour, endTime.minute);
    print(tempStart);
    print(tempEnd);
    //If the Task contains an reminder
    if (task.remind != null) {
      DateTime tempReminder = tempStart
          .subtract(ReminderModel.ReminderList()[task.remind!].duration);
      notificationModel = await _addTaskNotificationToDb(task: task);

      NotifyHelper.scheduledNotification(
          id: notificationModel.id!.toInt(),
          task: task,
          type: 'reminder',
          time: tempReminder);
    }

    //Add a timer to the date Base schedule  and set an reminder
    notificationModel = await _addTaskNotificationToDb(task: task);
    NotifyHelper.scheduledNotification(
        id: notificationModel.id!.toInt(),
        task: task,
        type: 'startTime',
        time: tempStart);

    notificationModel = await _addTaskNotificationToDb(task: task);
    task.endTaskNotificationId = notificationModel.id;
    await DBHelper.updateEndTaskNotificationId(
        task.id!, task.endTaskNotificationId!);

    NotifyHelper.scheduledNotification(
        id: notificationModel.id!.toInt(),
        task: task,
        type: 'endTime',
        time: tempEnd);
  }

  _addTaskNotificationToDb({required TaskModel task}) async {
    LocalNotification notification = LocalNotification(
        title: task.taskName,
        body: task.description,
        date: task.date,
        task_id: task.id);
    var value = await addNotification(notification: notification);
    notification.id = value;
    return notification;
  }

  Future<int> addNotification({notification}) async {
    return await DBHelper.insertNotification(notification);
  }

  Future<void> deleteTask(TaskModel task) async {
    var result = await DBHelper.queryTaskNotifications(task);

    result.forEach((row) async {
      await NotifyHelper.cancelNotifications(row['id']);
      await DBHelper.deleteNotificationDB(row['id']);
    });
    DBHelper.deleteTask(task);
    getTasksByDate();
    emit(DeleteTaskSuccessState());
  }
}
