part of 'schedule_cubit.dart';

@immutable
sealed class ScheduleState {}

final class ScheduleInitial extends ScheduleState {}

final class ChangeShowAll extends ScheduleState {}

final class ChangeCalendarFormat extends ScheduleState {}

final class ChangeDay extends ScheduleState {}

final class OnPageChanged extends ScheduleState {}

final class ChangSwitcher extends ScheduleState {}

final class ChangReminderOption extends ScheduleState {}

final class GetDateFromUser extends ScheduleState {}

final class GetTimeFromUser extends ScheduleState {}

final class GetTaskByDate extends ScheduleState {}

final class GetAllTask extends ScheduleState {}


final class DeleteTaskSuccessState extends ScheduleState {}


