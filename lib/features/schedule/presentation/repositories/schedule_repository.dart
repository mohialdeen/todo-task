import 'package:dartz/dartz.dart';
import 'package:task_manager/core/api_services/api_consumer.dart';
import 'package:task_manager/core/errors/exceptions.dart';
import 'package:task_manager/core/export/app_export.dart';
import 'package:task_manager/features/auth/data/models/user_model.dart';
import 'package:task_manager/features/schedule/data/models/task_model.dart';

class ScheduleRepository {
  final ApiConsumer api;

  ScheduleRepository({required this.api});


}
