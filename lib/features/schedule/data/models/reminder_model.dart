class ReminderModel
{
  ReminderModel(this.id,this.titleEn,this.titleAr,this.duration);
  int id ;
  String titleEn;
  String titleAr;
  Duration  duration;

  static List<ReminderModel> ReminderList() {
    return <ReminderModel>[
      ReminderModel(1, "5 Minutes Before", "قبل 5 دقائق",const Duration(minutes: 5)),
      ReminderModel(2, "15 Minutes Before", "قبل 15 دقيقة",const Duration(minutes: 15)),
      ReminderModel(2, "30 Minutes Before", "قبل 30 دقيقة",const Duration(minutes: 30)),
      ReminderModel(3, "1 Hour Before", "قبل ساعة واحدة",const Duration(hours: 1)),
      ReminderModel(4, "4 Hour Before", "قبل 4 ساعات",const Duration(hours: 4)),
      ReminderModel(5, "1 Day Before", "قبل يوم واحد",const Duration(days: 1)),
      ReminderModel(6, "2 Day Before", "قبل يومين",const Duration(days: 2)),
      ReminderModel(7, "1 Week Before", "قبل اسبوع",const Duration(days: 7)),

    ];



  }
}