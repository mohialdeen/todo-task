class LocalNotification
{
  int? id;
  String? title;
  String? body;
  String? date;
  int? task_id;

  LocalNotification.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.title = json['title'];
    this.body = json['body'];
    this.date = json['date'];
    this.task_id = json['task_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['body'] = this.body;
    data['date'] = this.date;
    data['task_id'] = this.task_id;
    return data;
  }

  LocalNotification({this.id, this.title, this.body, this.date, this.task_id});
}