import 'dart:convert';

class TaskModel {
  int? id;
  String? taskName;
  String? description;
  String? date;
  String? startTime;
  int? customTask;
  String? endTime;
  int? remind;
  int? endTaskNotificationId;

  TaskModel.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.endTaskNotificationId = json['finishNotificationId'];
    this.taskName = json['taskName'].toString();
    this.description = json['description'].toString();
    this.date = json['date'];
    this.startTime = json['startTime'];
    this.endTime = json['endTime'];
    this.remind = json['remind'];
    this.customTask = json['customTask'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['finishNotificationId'] = this.endTaskNotificationId;
    data['taskName'] = this.taskName;
    data['description'] = this.description;
    data['date'] = this.date;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    data['remind'] = this.remind;
    data['customTask'] = this.customTask;
    return data;
  }

  factory TaskModel.fromJsonString(String str) => TaskModel.fromJson(jsonDecode(str));

  String toJsonString() => jsonEncode(toJson());

  TaskModel(
      {this.id,
      this.taskName,
      this.description,
      this.date,
      this.startTime,
      this.endTime,
      this.remind,
      this.customTask,
      });
}
