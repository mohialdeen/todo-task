import 'package:task_manager/features/to_do/presentation/view/to_do.dart';

import '../../../../core/export/app_export.dart';
import '../../../schedule/presentation/view/schedule.dart';

class MainHome extends StatefulWidget {
  MainHome({super.key});

  @override
  State<MainHome> createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  @override
  void initState() {
    super.initState();
  }

  List<Widget> screens = [
    ToDo(),
    Schedule(),
  ];

  List<IconData> listOfIcons = [
    AppIcons.done_outlined,
    AppIcons.schedule_outlined,
  ];
  List<IconData> listOfIconsFill = [
    AppIcons.done_filled,
    AppIcons.schedule_filled,
  ];

  var _currentIndex = 0;
  var pageController = PageController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: AppColors.background,
          border: Border(
            top: BorderSide(width: 0.50, color: AppColors.secondary),
          ),
        ),
        child: IntrinsicHeight(
          child: Padding(
            padding:
                EdgeInsetsDirectional.symmetric(horizontal: size.width * .0422),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: List.generate(
                2,
                (index) => InkWell(
                  onTap: () {
                    setState(() {
                      _currentIndex = index;
                      pageController.jumpToPage(index);
                    });
                  },
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeInToLinear,
                        margin: EdgeInsetsDirectional.only(
                          bottom:
                              index == _currentIndex ? 0 : size.width * .035,
                        ),
                        width: size.width * .150,
                        height: index == _currentIndex ? size.width * .007 : 0,
                        decoration: BoxDecoration(
                          color: AppColors.accent,
                          borderRadius: BorderRadius.vertical(
                            bottom: Radius.circular(5.r),
                          ),
                        ),
                      ),
                      Icon(
                        index == _currentIndex
                            ? listOfIconsFill[index]
                            : listOfIcons[index],
                        size: 32.w,
                        color: index == _currentIndex
                            ? AppColors.accent
                            : AppColors.secondary,
                      ),
                      Gap(size.width * .02),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      body: PageView(
        controller: pageController,
        children: screens,
        onPageChanged: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}
