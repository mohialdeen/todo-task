import 'package:dartz/dartz.dart';
import 'package:task_manager/core/api_services/api_consumer.dart';
import 'package:task_manager/core/errors/exceptions.dart';
import 'package:task_manager/core/export/app_export.dart';
import 'package:task_manager/features/auth/data/models/user_model.dart';

class AuthRepository {
  final ApiConsumer api;

  AuthRepository({required this.api});

  /// Returns either a [UserModel] if sign in is successful or a [String] error message if sign in fails.

  Future<Either<String, UserModel>> login({
    required String username,
    required String password,
  }) async {
    try {
      final response = await api.post(
        EndPoint.logIn,
        data: {
          "username": username,
          "password": password,

        },
      );

      UserModel user = UserModel.fromJson(response);

      CacheHelper.saveData(key: AppKey.token, value: user.token);
      CacheHelper.saveData(key: AppKey.refreshToken, value: user.refreshToken);
      CacheHelper.saveData(key: AppKey.username, value: user.username);
      CacheHelper.saveData(key: AppKey.firstName, value: user.firstName);
      CacheHelper.saveData(key: AppKey.lastName, value: user.lastName);
      CacheHelper.saveData(key: AppKey.gender, value: user.gender);
      CacheHelper.saveData(key: AppKey.email, value: user.email);
      CacheHelper.saveData(key: AppKey.image, value: user.image);
      CacheHelper.saveData(key: AppKey.userId, value: user.id);
      return Right(user);
    } on ServerException catch (e) {
      return Left(e.errModel.error.toString());
    }
  }
}
