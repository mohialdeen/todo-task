import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:task_manager/core/app_colors/app_colors.dart';
import 'package:task_manager/core/app_styles/app_fonts.dart';
import 'package:task_manager/core/functions/custom_snack_bar.dart';
import 'package:task_manager/core/widgets/custom_elevated_button.dart';
import 'package:task_manager/core/widgets/custom_enter_data.dart';
import 'package:task_manager/features/main_home/presentation/view/main_home.dart';
import 'package:task_manager/features/schedule/presentation/view/schedule.dart';
import 'package:task_manager/features/to_do/presentation/view_model/to_do_cubit.dart';

import '../../../../core/export/app_export.dart';
import '../view_model/auth_cubit.dart';

class LogInScreen extends StatelessWidget {
  LogInScreen({super.key});

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var tr = S.of(context);
    TextEditingController userNameController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    return BlocConsumer<AuthCubit, AuthState>(listener: (context, state) async {
      if (state is LogInSuccessState) {
        customSnackBar(
            context: context,
            label: tr.welcomeBack,
            color: AppColors.secondaryAccent,
            fontColor: AppColors.accentFontColor);
        ToDoCubit.get(context).getToDo(isRefresh: true);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => MainHome(),
          ),
        );
      }
      if (state is LogInErrorState) {
        customSnackBar(
            context: context,
            label: state.error,
            color: AppColors.secondaryRed,
            fontColor: AppColors.redFontColor);
      }
    }, builder: (context, state) {
      var cubit = AuthCubit.get(context);
      return SafeArea(
        child: Scaffold(
          body: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Gap(64.h),
                  Center(
                    child: AnimatedTextKit(
                      animatedTexts: [
                        TyperAnimatedText(
                          tr.welcomeBack,
                          textStyle: boldFont700(
                              color: AppColors.accent, fontSize: 25.w),
                        ),
                      ],
                      onFinished: () {},
                      totalRepeatCount: 1,
                      displayFullTextOnTap: true,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    child: Center(
                      child: AnimatedTextKit(
                        animatedTexts: [
                          TyperAnimatedText(
                              tr.log_in_and_start_managing_your_tasks,
                              textStyle: mediumFont500(
                                  color: AppColors.black, fontSize: 20.w),
                              textAlign: TextAlign.center),
                        ],
                        onFinished: () {},
                        totalRepeatCount: 1,
                        displayFullTextOnTap: true,
                      ),
                    ),
                  ),
                  Image.asset(
                    'assets/images/login_task.png',
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomEnterData(
                          padding: EdgeInsets.symmetric(vertical: 8.h),
                          title: tr.user_name,
                          hintText: tr.enter_user_name,
                          textStyle: regularFont400(
                              color: AppColors.secondary, fontSize: 16.w),
                          titleStyle: mediumFont500(
                              color: AppColors.secondary, fontSize: 16.w),
                          filled: true,
                          focusedColor: AppColors.primary,
                          filledColor: AppColors.secondary,
                          fillColor: AppColors.gray,
                          border: false,
                          controller: userNameController,
                        ),
                        CustomEnterData(
                          controller: passwordController,
                          padding: EdgeInsets.symmetric(vertical: 8.h),
                          title: tr.password,
                          hintText: tr.enter_password,
                          suffix: Icon(cubit.passwordVisibility
                              ? Icons.visibility_outlined
                              : Icons.visibility_off_outlined),
                          obscureText: cubit.passwordVisibility,
                          suffixFunction: () {
                            cubit.showPassword();
                          },
                          textStyle: regularFont400(
                              color: AppColors.secondary, fontSize: 16.w),
                          titleStyle: mediumFont500(
                              color: AppColors.secondary, fontSize: 16.w),
                          filled: true,
                          focusedColor: AppColors.primary,
                          filledColor: AppColors.secondary,
                          fillColor: AppColors.gray,
                          border: false,
                        ),
                      ],
                    ),
                  ),
                  Gap(24.h),
                  state is LogInLoadingState
                      ? const CircularProgressIndicator()
                      : CustomElevatedButton(
                          margin: EdgeInsets.symmetric(
                              vertical: 8.h, horizontal: 16.w),
                          onPressed: () {
                            print(passwordController.text);
                            if (_formKey.currentState!.validate()) {
                              context.read<AuthCubit>().logIn(
                                    password: passwordController.text,
                                    userName: userNameController.text,
                                  );
                            }
                          },
                          height: 46.h,
                          text: tr.log_in,
                          buttonTextStyle: boldFont700(
                              fontSize: 16.w, color: AppColors.white),
                          buttonStyle: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.w),
                            ),
                            backgroundColor: AppColors.accent,
                          ),
                        ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
