part of 'auth_cubit.dart';

@immutable
sealed class AuthState {}

final class AuthInitial extends AuthState {}

final class ShowPasswordState extends AuthState {}


final class LogInErrorState extends AuthState {
  final String error;

  LogInErrorState(this.error);
}

final class LogInLoadingState extends AuthState {}

final class LogInSuccessState extends AuthState {}
