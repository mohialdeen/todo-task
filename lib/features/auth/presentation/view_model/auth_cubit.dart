import 'package:bloc/bloc.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:task_manager/features/auth/presentation/repositories/auth_repository.dart';
import '../../../../core/export/app_export.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepository authRepository;

  bool passwordVisibility = false;

  AuthCubit(this.authRepository) : super(AuthInitial());

  static AuthCubit get(context) => BlocProvider.of(context);

  void showPassword() {
    passwordVisibility = !passwordVisibility;
    emit(ShowPasswordState());
  }


  logIn({
    required String userName,
    required String password,
  }) async {
    emit(LogInLoadingState()); // Emit loading state while Log in is in progress.
    // Call the logIn function from the AuthRepository to attempt user Log in.
    final result = await authRepository.login(
      username: userName,
      password: password,
    );
    // Handle the Log in result.
    result.fold(
        // If Log in fails, emit logInErrorState with the error message.
        (l) => emit(LogInErrorState(l)),
        // If Log in is successful, emit logInSuccessState.
        (r) {
      return emit(LogInSuccessState());
    });
  }
}
