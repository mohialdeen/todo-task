abstract class ProfileState {}

final class ProfileInitial extends ProfileState {}

final class SelectImageState extends ProfileState{}

final class GetProfileReviewsLoadingState extends ProfileState {}

final class GetProfileReviewsSuccessState extends ProfileState {}

final class GetProfileReviewsErrorState extends ProfileState {
  final String error;

  GetProfileReviewsErrorState(this.error);
}

