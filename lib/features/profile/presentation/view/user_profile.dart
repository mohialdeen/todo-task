import 'package:task_manager/core/widgets/custom_icon_button.dart';
import 'package:task_manager/core/widgets/custom_second_app_bar.dart';
import 'package:task_manager/core/widgets/icon_text.dart';
import 'package:task_manager/core/widgets/profile_avatar.dart';
import 'package:task_manager/features/profile/presentation/view_model/profile_cubit.dart';

import '../../../../../core/export/app_export.dart';
import '../view_model/profile_state.dart';

class UserProfile extends StatelessWidget {
  const UserProfile({super.key});

  @override
  Widget build(BuildContext context) {
    var t = S.of(context);
    var cubit = ProfileCubit.get(context);
    return BlocConsumer<ProfileCubit, ProfileState>(
      listener: (context, state) {},
      builder: (context, state) {
        return Scaffold(
          appBar: CustomSecondAppBar(
            title: t.profile,
            actions: [],
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsetsDirectional.symmetric(vertical: 12.h),
                  child: Column(
                    children: [
                      ProfileAvatar(
                        userName: CacheHelper.getData(key: AppKey.username),
                        size: 148.w,
                        image: CacheHelper.getData(key: AppKey.image),
                      ),
                      Gap(8.h),
                      Text(CacheHelper.getData(key: AppKey.username),
                          style: mediumFont500(
                              fontSize: 20, color: AppColors.primary)),
                    ],
                  ),
                ),
                Column(
                  children: [
                    IconTextApp(
                      text: t.full_name,
                      textTwo: CacheHelper.getData(key: AppKey.firstName) +
                          " " +
                          CacheHelper.getData(key: AppKey.lastName),
                      isColum: true,
                      devider: true,
                      iconColor: AppColors.primary,
                      showSplash: true,
                      icon: AppIcons.user,
                      styleTwo: regularFont400(
                          color: AppColors.secondary, fontSize: 16.w),
                    ),
                    IconTextApp(
                      icon: Icons.email_outlined,
                      text: t.email,
                      devider: true,
                      textTwo: CacheHelper.getData(key: AppKey.email),
                      isColum: true,
                      iconColor: AppColors.primary,
                      showSplash: true,
                    ),
                    IconTextApp(
                      icon: Icons.transgender_outlined,
                      text: t.gender,
                      textTwo: CacheHelper.getData(key: AppKey.gender),
                      isColum: true,
                      iconColor: AppColors.primary,
                      showSplash: true,
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
