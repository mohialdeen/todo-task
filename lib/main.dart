import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:task_manager/core/api_services/dio_consumer.dart';
import 'package:task_manager/core/languages_manager/language_constants.dart';
import 'package:task_manager/features/auth/presentation/view_model/auth_cubit.dart';
import 'package:task_manager/features/main_home/presentation/view/main_home.dart';
import 'package:task_manager/features/profile/presentation/view_model/profile_cubit.dart';
import 'package:task_manager/features/schedule/presentation/repositories/schedule_repository.dart';
import 'package:task_manager/features/schedule/presentation/view/schedule.dart';
import 'package:task_manager/features/schedule/presentation/view_model/schedule_cubit.dart';
import 'package:task_manager/features/settings/presentation/view-model/settings_cubit.dart';
import 'package:task_manager/features/to_do/presentation/repositories/to_do_repository.dart';
import 'package:task_manager/features/to_do/presentation/view_model/to_do_cubit.dart';

import 'core/app_colors/app_colors.dart';
import 'core/app_constance/app_constance.dart';
import 'core/export/app_export.dart';
import 'core/theme/theme.dart';
import 'features/auth/presentation/repositories/auth_repository.dart';
import 'features/auth/presentation/view/log_in_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await CacheHelper.init();
  await DBHelper.initDB();
  await NotifyHelper.initializeNotification();

  Bloc.observer = MyBlocObserver();
  CacheHelper.getData(key: AppKey.languageCode) == null
      ? CacheHelper.saveData(
          key: AppKey.languageCode, value: Platform.localeName.substring(0, 2))
      : null;
  CacheHelper.getData(key: "isLightTheme") == null
      ? CacheHelper.saveData(
          key: "isLightTheme",
          value: SchedulerBinding.instance.platformDispatcher.platformBrightness
                      .toString() ==
                  "Brightness.light"
              ? true
              : false)
      : null;

  runApp(TaskManager());
}

class TaskManager extends StatefulWidget {
  TaskManager({super.key});

  static void setLocale(BuildContext context, Locale newLocale) {
    _TaskManagerState? state =
        context.findAncestorStateOfType<_TaskManagerState>();
    state?.setLocale(newLocale);
  }

  static late String fcmToken;

  @override
  State<TaskManager> createState() => _TaskManagerState();
}

class _TaskManagerState extends State<TaskManager> {
  Locale? _locale;

  @override
  void dispose() {
    themeManager.removeListener(themeListener);
    super.dispose();
  }

  @override
  void initState() {
    themeManager.addListener(themeListener);
    super.initState();
  }

  themeListener() {
    if (mounted) {
      setState(() {});
    }
  }

  setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  void didChangeDependencies() {
    getLocale().then((locale) => {setLocale(locale)});
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: AppColors.background,
      systemNavigationBarColor: AppColors.background,
    ));
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              AuthCubit(AuthRepository(api: DioConsumer(dio: Dio()))),
        ),
        BlocProvider(
          create: (context) =>
              ScheduleCubit(ScheduleRepository(api: DioConsumer(dio: Dio())))..getTasksByDate(),
        ),
        BlocProvider(
          create: (context) => ProfileCubit(),
        ),
        BlocProvider(
          create: (context) => SettingsCubit(),
        ),
        BlocProvider(
          create: (context) =>
              ToDoCubit(ToDoRepository(api: DioConsumer(dio: Dio())))..getToDoFromTable(),
        ),
      ],
      child: ScreenUtilInit(
        designSize: Size(360, 800),
        minTextAdapt: true,
        splitScreenMode: true,
        child: MaterialApp(
          localizationsDelegates: [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
          supportedLocales: S.delegate.supportedLocales,
          locale: _locale,
          debugShowCheckedModeBanner: false,
          theme: lightTheme,
          darkTheme: darkTheme,
          themeMode: themeManager.themeMode,
          home: CacheHelper.getData(key: AppKey.token) != null
              ? MainHome()
              : LogInScreen(),
        ),
      ),
    );
  }
}
