# ToDo Task App

## Overview

ToDo Task App is a Flutter project designed to offer users task management
experience. It integrates a variety of features, including user authentication, task management,
pagination, state management, local storage, and unit tests. Furthermore, it provides language
localization, dark mode, light mode, user profile management, and a dedicated section for scheduling
tasks.

## Key Features

### Main Features

#### User Authentication

- Secure login using Username and Password.
- Utilizes [dummyjson.com](https://dummyjson.com/docs/auth) for authentication.
- Seamless token renewal with refresh token handling.

#### Task Management

- CRUD operations for tasks: view, add, edit, delete.
- Data management through [dummyjson.com](https://dummyjson.com/docs/todos) API.

#### State Management

- Implemented with the BLoC pattern using Cubit.

#### Local Storage

- Persistence using SQFlite for SQLite databases in Flutter.
- Tasks accessible even when the app is closed.

#### Pagination

- Efficient pagination for large task lists.

### Additional Features

#### Language Support

- Applying localization to switch between English and arabic.

#### Dark Mode and Light Mode

- Customizable UI with dark and light mode support.

#### User Profile

- View and edit user profile within the app.
- Profile information fetched from the backend server.

#### Schedule Section

- Dedicated task management section called "Schedule."
- Detailed task management with reminder functionality.
- Local storage for efficient task management.

### Sub Features

#### Save Tasks

- Ability to save tasks for quick access.
- View saved tasks separately for easy reference.

## Screenshots

Here are some screenshots of the app:

### Login Screen

<img src="assets/screen_shots/photo_7_2024-06-10_21-01-16.jpg" width="250">

### ToDo Management Screen

<img src="assets/screen_shots/photo_6_2024-06-10_21-01-16.jpg" width="250">


<img src="assets/screen_shots/photo_13_2024-06-10_21-01-16.jpg" width="250">


<img src="assets/screen_shots/photo_14_2024-06-10_21-01-16.jpg" width="250">


<img src="assets/screen_shots/photo_12_2024-06-10_21-01-16.jpg" width="250">

### Profile Screen
<img src="assets/screen_shots/photo_1_2024-06-10_21-01-16.jpg" width="250">


<img src="assets/screen_shots/photo_8_2024-06-10_21-01-16.jpg" width="250">

### Schedule Screen

<img src="assets/screen_shots/photo_11_2024-06-10_21-01-16.jpg" width="250">


<img src="assets/screen_shots/photo_3_2024-06-10_21-01-16.jpg" width="250">


<img src="assets/screen_shots/photo_2_2024-06-10_21-01-16.jpg" width="250">


## Login Information
To log in to the application, use the following credentials:

- **Username:** `emilys`
- **Password:** `emilyspass`








